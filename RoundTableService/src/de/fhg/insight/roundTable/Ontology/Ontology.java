package de.fhg.insight.roundTable.Ontology;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Ontology{



public static void main (String args[]){
	System.out.print(getRandomLabel("glossary.xml"));
}

public static String getRandomLabel(String dicPath ){
    // parse dic
	String label = null;
    try {
	File dic = new File(dicPath);
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(dic);
	doc.getDocumentElement().normalize();

    NodeList labelNodes = doc.getElementsByTagName("label");
	Node node = labelNodes.item((int)(Math.random()*labelNodes.getLength()));
	label = node.getTextContent();
	
    }
    catch(Exception ex){
    	ex.printStackTrace();
    }
    return label;

}


}