package de.fhg.insight.roundTable;

import de.fhg.insight.roundTable.Types.EventData;
import de.fhg.insight.roundTable.Types.LocationData;
import de.fhg.insight.roundTable.Types.TimeData;

public abstract class IRoundTableData {
	public abstract EventData getEvent();

	public abstract TimeData getTime();

	public abstract LocationData getLocation();

}
