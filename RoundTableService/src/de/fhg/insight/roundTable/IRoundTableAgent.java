package de.fhg.insight.roundTable;



/**
 * Interface for ...
 * 
 * @author abablok
 * @author sbothe
 * 
 */
public interface IRoundTableAgent {

	/**
	 * blubb
	 * 
	 * @param t
	 */
	public void joinTable(IRoundTable table);

	/**
	 * @return
	 */
	public String getInfo();

	/**
	 * Bla...
	 * 
	 */
	public void leaveTable();

	/**
	 * @param someData
	 * @param t
	 */
	public void callTable(IRoundTableData someData, IRoundTable table);

	/**
	 * Collect Information on the given Data, i.e. search for related Data
	 */
	void collectInformation();

	/**
	 * Put Data for discussion on round table
	 */
	void cardsOn();

	/**
	 * @param d
	 */
	public void addData(IRoundTableData d);

	/**
	 * @param issue
	 * @param d
	 */
	void rate_data(IRoundTableData issue, IRoundTableData d);

}