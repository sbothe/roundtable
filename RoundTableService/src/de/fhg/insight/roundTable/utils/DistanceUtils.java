package de.fhg.insight.roundTable.utils;

public final class DistanceUtils {
	static public double euklidian(final double a, final double b,
			final double c) {
		return Math.sqrt(a * a + b * b + c * c);
	}

	static public double euklidian(final double a, final double b) {
		return Math.sqrt(a * a + b * b);
	}
}
