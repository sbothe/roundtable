package de.fhg.insight.roundTable;

public interface IRoundTable {
	public boolean addAgent(IIsaAgent a);

	public IRoundTableData discuss();

}
