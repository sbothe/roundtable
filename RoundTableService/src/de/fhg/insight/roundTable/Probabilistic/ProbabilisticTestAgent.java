package de.fhg.insight.roundTable.Probabilistic;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import de.fhg.insight.roundTable.IRoundTable;
import de.fhg.insight.roundTable.IRoundTableAgent;
import de.fhg.insight.roundTable.IRoundTableData;


/**
*
* @author fschnitzler
* @author tliebig
*
*/
public class ProbabilisticTestAgent implements IRoundTableAgent {
	
	private boolean callingAgent = false;
	private final LinkedList<ProbabilisticRoundTableData> data = new LinkedList<ProbabilisticRoundTableData>(); // muss
	// dynamisches
	// Array
	// sein,
	// oder
	// liste.
	// private TreeMap<Data,Number> relevant_data = new TreeMap<Data,Number>();
	private final LinkedList<ProbabilisticRoundTableData> relevant_data = new LinkedList<ProbabilisticRoundTableData>();
	ProbabilisticRoundTable participating_at;
	private final String agentID; // description of the sensor we are dealing
									// with

	public ProbabilisticTestAgent() {
		// TODO: find a right way to determine an ID..
		this.agentID = "Agent_" + (int) (Math.random() * 1000);

	}

	public ProbabilisticTestAgent(ProbabilisticRoundTableData sensorData, String agentID) {
		this.agentID = agentID;
		this.data.add(sensorData);
	}

	@Override
	public String getInfo() {
		return agentID;
	}

	@Override
	public void joinTable(IRoundTable t) {
		this.participating_at = (ProbabilisticRoundTable) t;
		participating_at.addAgent(this);
		System.out.println("Agent for " + this.getInfo() + " joined");
	}

	@Override
	public void leaveTable() {
		participating_at.deleteAgent(this);
	}

	@Override
	public void callTable(IRoundTableData iData, IRoundTable it) {
		ProbabilisticRoundTableData someData = (ProbabilisticRoundTableData) iData ;
		ProbabilisticRoundTable t = (ProbabilisticRoundTable) it;
		try {
			t.setIssue(someData);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.callingAgent = true;
		this.joinTable(t);
	}

	public ProbabilisticRoundTableData mostRelevant() {
//		ProbabilisticRoundTableData i = null;
//		double rel = 200;
//		for (ProbabilisticRoundTableData d : relevant_data) {
//			if (d.get_relevance() <= rel) {
//				i = d;
//				rel = i.get_relevance();
//			}
//		}
//
//		return i;
		return null;
	}

	// Collect Information on the given Data, i.e. search for related Data
	@Override
	public void collectInformation() {
//		double boundary = 4;
//		double d_date;
//		double d_event;
//		double d_loc;
//		if (!this.callingAgent) {
//			for (ProbabilisticRoundTableData d : data) {
//
//				if (participating_at.getIssue().getEvent() < 0) {
//					d_event = 0;
//					boundary--;
//				} else {
//					d_event = d.getEvent()
//							- participating_at.getIssue().getEvent();
//				}
//
//				if (participating_at.getIssue().getLocation() < 0) {
//					d_loc = 0;
//					boundary--;
//				} else {
//					d_loc = d.getLoc()
//							- participating_at.getIssue().getLocation();
//				}
//				long t = d.getDate().getTime().getTime();
//				long x = participating_at.getIssue().getDate().getTime()
//						.getTime();
//				d_date = (t - x) / (60. * 60 * 1000);
//				// System.out.println(d_date);
//				d.change_relevance(euklidian(d_event, d_loc, d_date));
//
//				// System.out.println("Relevanz: "+d.get_relevance());
//				if (d.get_relevance() < boundary) {
//					// relevant_data.put(d,d.get_relevance());
//					this.relevant_data.add(d);
//				}
//
//			}
//		}

	}

	// Put Data for discussion on round table
	@Override
	public void cardsOn() {
		if (!this.callingAgent) {
			System.out.println(this.getInfo() + " says "
					+ this.getRelevantInf());
			ProbabilisticEventData data = new ProbabilisticEventData();
			
			data.put(new ProbabilisticEventType("storm"),0.1);
			data.put(new ProbabilisticEventType("flood"),0.4);
			data.put(new ProbabilisticEventType("no internet"),0.9);
			int randomLocation = (int) ((Math.random() * 9) - 2);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			ProbabilisticRoundTableData event = new ProbabilisticRoundTableData(sdf, randomLocation, data);
			
			System.out.println(event);
			// TODO: set data here, include probabilities!!
			//participating_at.addData(this, this.mostRelevant());
			participating_at.addData(this, event);
		}
	}

	@Override
	public void addData(IRoundTableData d) {
		data.add((ProbabilisticRoundTableData) d);
	}

	public String getRelevantInf() {
		try {
			// return this.relevant_data.firstKey().getDataInf();
			return this.mostRelevant().toString();

		} catch (NoSuchElementException e) {
			return "\"nothing to say\"";
		} catch (IndexOutOfBoundsException e) {
			return "\"nothing to say\"";
		} catch (NullPointerException e) {
			return "\"nothing to say\"";
		}
		// return "";
	}

	public double euklidian(double a, double b, double c) {
		return Math.sqrt(a * a + b * b + c * c);
	}

	public double euklidian(double a, double b) {
		return Math.sqrt(a * a + b * b);
	}

	@Override
	public void rate_data(IRoundTableData issue, IRoundTableData d) {
		/*
		 * double min=0; int loc; int event; int i,j; loc=issue.getLoc();
		 * event=issue.getEvent();
		 * for(i=event-(int)issue.get_range_event()/2;i<=
		 * event+(int)issue.get_range_event()/2;i++){ for(j=loc-(int)
		 * issue.get_range_loc()/2;j<=loc+(int)issue.get_range_loc()/2;j++){
		 *
		 * if(min<euklidian(i,j)){ } } }
		 */
	}

}