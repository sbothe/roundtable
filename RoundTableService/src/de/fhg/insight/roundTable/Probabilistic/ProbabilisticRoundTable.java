package de.fhg.insight.roundTable.Probabilistic;

import java.util.HashMap;
import java.util.LinkedList;
//import java.util.Map;
//import java.util.Calendar;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;







import de.fhg.insight.roundTable.IRoundTable;
import de.fhg.insight.roundTable.IRoundTableAgent;
import de.fhg.insight.roundTable.IRoundTableData;
import de.fhg.insight.roundTable.Samples.RoundTableDataExample;

/**
 * 
 * @author fschnitzler
 * @author tliebig
 * 
 * This class aggregates probabilistic data (conditionnal densities) from an agent.
 */
public class ProbabilisticRoundTable  implements IRoundTable{

	private final LinkedList<IRoundTableAgent> participants = new LinkedList<IRoundTableAgent>();
	private final HashMap<IRoundTableAgent, ProbabilisticRoundTableData> OnTableData = new HashMap<IRoundTableAgent, ProbabilisticRoundTableData>();
	private ProbabilisticRoundTableData result = null;
	private ProbabilisticRoundTableData issue = null;
	// TODO: assumption: all possible labels are provided by the agent calling for the round table

	// private TreeSet<Calendar> data_times = new TreeSet<Calendar>;

	// -----Konstruktor-----
	public ProbabilisticRoundTable() {
	}

	// -------Methods-------

	// add an Agent to the Table
	public boolean addAgent(IRoundTableAgent a) {
		return ((this.participants.add(a)));
	}

	public void addData(IRoundTableAgent a, ProbabilisticRoundTableData d) {
		OnTableData.put(a, d);
	}

	// delete an Agent (if he is useless or whatever)
	public boolean deleteAgent(IRoundTableAgent a) {
		if ((this.participants.remove(a))) {
			System.out.println("Something failed removing the Agent for"
					+ a.getInfo());
			return false;
		}
		return true;
	}

	// set the Issue to discuss based on given data
	public void setIssue(ProbabilisticRoundTableData rtData)
			throws CloneNotSupportedException {
		if (this.issue == null) {
			this.issue = rtData;
			this.result = new ProbabilisticRoundTableData(rtData);
		} else {
			System.out
					.println("You are not allowed to set a new Issue (because we are just discussing one)");
		}
		this.result=rtData;
	}

	public ProbabilisticRoundTableData getIssue() {
		return this.issue;
	}

	// discuss the information provided by the agents
	// At the moment: the round table uses the best semantic information
	public IRoundTableData discuss() {
		//tells the ISA to get ready
		for (IRoundTableAgent a : participants) {
			a.collectInformation();
		}
		//gets the data
		for (IRoundTableAgent a : participants) {
			a.cardsOn(); // should crush data
		}
		//aggregate information
		ProbabilisticEventData resultProba = naiveBayes();
//		double acc;
//		for (RoundTableDataExample d : OnTableData.values()) {
//			if (!(d == null)) {
//				acc = result.getAccuracy();
//				if (d.getRange_event() < result.getRange_event()) {
//					result.setEvent(d.getEvent());
//				}
//			}
//		}
		result.setEventData(resultProba);
		
		return null;
	}
	
	//TODO: use log likelihood
	public ProbabilisticEventData naiveBayes(){
		//create prior
		ProbabilisticEventData update = new ProbabilisticEventData();
		//TODO: fix this
		//ProbabilisticEventData initial = OnTableData.get(OnTableData.values().iterator().next()).getEventData();
		//update.putAll(initial);
		update.put(new ProbabilisticEventType("storm"),0.1);
		update.put(new ProbabilisticEventType("flood"),0.4);
		update.put(new ProbabilisticEventType("no internet"),0.9);
		update.setUniform(); //to do
		
		//perform updates
		//integrate likelihoods
		for (ProbabilisticRoundTableData d : OnTableData.values()) {
			ProbabilisticEventData data = d.getEventData();
			System.out.println("processing " + data);
			for (ProbabilisticEventType event : update.keySet()) {
//				for (ProbabilisticEventType eventToTest : data.keySet()) {
//					if (eventToTest.equals(event)){
//						System.out.println("success!");
//						Double likelihood = data.get(event);
//					}
//				}
				Double likelihood = data.get(event);
				if (likelihood==null)
					likelihood = 0.0;
				Double postLikelihood= likelihood * update.get(event);
				update.put(event, postLikelihood);
			}
			
			//TODO: move this one
			Double sum = 0.0;
			for (ProbabilisticEventType event : update.keySet()) {
				sum += update.get(event);
			}	
			if (sum>0){
				for (ProbabilisticEventType event : update.keySet()) {
					update.put(event, update.get(event) / sum);
				}
			}
			
			//display
			System.out.println("probability of events:" + update);
		}
//			if (!(d == null)) {
//				acc = result.getAccuracy();
//				if (d.getRange_event() < result.getRange_event()) {
//					result.setEvent(d.getEvent());
//				}
//			}
//		}
		//normalize
		
		return update;
	}

	public void DataOnTable() {
		System.out.println("What lies on table: ");
		for (IRoundTableAgent a : OnTableData.keySet()) {
			try {
				System.out.println(OnTableData.get(a).toString());
			} catch (NullPointerException e) {
				System.out.println("Nothing");
			}
		}

	}

	public void result() {
		System.out.println("The result of the discussion is "
				+ result.toString());
	}
}