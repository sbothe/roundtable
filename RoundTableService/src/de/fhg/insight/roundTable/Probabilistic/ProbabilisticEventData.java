package de.fhg.insight.roundTable.Probabilistic;

import java.util.HashMap;

public class ProbabilisticEventData extends HashMap<ProbabilisticEventType,Double>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void setUniform() {
		Integer numberLabels = this.keySet().size();
		for (ProbabilisticEventType event : this.keySet()) {
			this.put(event, 1. / numberLabels);
		}
	}

}
