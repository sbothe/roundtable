package de.fhg.insight.roundTable.Probabilistic;

//TODO: use 
// import apache.commons.lang3.builder.HashCodeBuilder

// Indentification for an event. node of the onthology
public class ProbabilisticEventType extends Object{
	String Label = null;
	
	public ProbabilisticEventType(String label) {
		super();
		Label = label;
	}
	@Override 
	public boolean equals(Object other) {
        if (!(other instanceof ProbabilisticEventType)) {
            return false;
        }
        ProbabilisticEventType otherProbabilisticEventType = (ProbabilisticEventType) other;
		return otherProbabilisticEventType.Label.equals(this.Label);
	}
	@Override
    public int hashCode() {
        return Label.hashCode();
    }
	@Override
	public String toString(){
		return this.Label;
	}
}
