package de.fhg.insight.roundTable.Probabilistic;

import java.text.SimpleDateFormat;

import de.fhg.insight.roundTable.IRoundTableData;

// This class contains events and associated P(obs | event)
// TODO: create a super class RoundTableData and call it event
public class ProbabilisticRoundTableData extends IRoundTableData{
	
	ProbabilisticEventData eventData = null; 
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private final int location;
	public ProbabilisticRoundTableData(SimpleDateFormat sdf,
			int location, ProbabilisticEventData eventData) {
		super();
		this.eventData = eventData;
		this.sdf = sdf;
		this.location = location;
	}
	public ProbabilisticRoundTableData(ProbabilisticRoundTableData rtData) {
		super();
		this.eventData = rtData.eventData;
		this.sdf = rtData.sdf;
		this.location = rtData.location;
	}
	public int getLocation() {
		return location;
	}
	public ProbabilisticEventData getEventData() {
		return eventData;
	}
	public void setEventData(ProbabilisticEventData eventData) {
		this.eventData = eventData;
	}
	public SimpleDateFormat getSdf() {
		return sdf;
	}
	public void setSdf(SimpleDateFormat sdf) {
		this.sdf = sdf;
	}

}
