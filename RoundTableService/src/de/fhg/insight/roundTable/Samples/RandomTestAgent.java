package de.fhg.insight.roundTable.Samples;

import java.util.ArrayList;
import java.util.List;

import de.fhg.insight.roundTable.RoundTableData;
import de.fhg.insight.roundTable.Samples.utils.generateRandomData;

public class RandomTestAgent extends ISATemplate {
	private final List<RoundTableData> isaData;

	public RandomTestAgent() {
		this.isaData = new ArrayList<RoundTableData>();
		for (int i = 0; i < 100; ++i) {
			isaData.add(generateRandomData.generateRoundTableData());
		}
	}
}
