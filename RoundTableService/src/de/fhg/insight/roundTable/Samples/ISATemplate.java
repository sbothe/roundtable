package de.fhg.insight.roundTable.Samples;

import java.util.ArrayList;
import java.util.List;

import de.fhg.insight.roundTable.IIsaAgent;
import de.fhg.insight.roundTable.RoundTableData;

public class ISATemplate implements IIsaAgent {
	private final List<RoundTableData> isaData;

	public ISATemplate() {
		this.isaData = new ArrayList<RoundTableData>();
	}

	@Override
	public RoundTableData getData(RoundTableData reference) {
		RoundTableData mostRelevant = null;
		double bestDistance = Double.POSITIVE_INFINITY;
		for (RoundTableData d : isaData) {
			if (reference.distance(d) < bestDistance) {
				bestDistance = reference.distance(d);
				mostRelevant = d;
			}
		}

		return mostRelevant;
	}
}
