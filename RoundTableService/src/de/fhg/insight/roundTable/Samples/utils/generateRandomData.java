package de.fhg.insight.roundTable.Samples.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import de.fhg.insight.roundTable.RoundTableData;
import de.fhg.insight.roundTable.Samples.Constants;
import de.fhg.insight.roundTable.Samples.RoundTableDataExample;

public class generateRandomData {

	/**
	 * Helper for generating some random sample data
	 * 
	 * @return some random data
	 */
	public static RoundTableData generateRoundTableData() {
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		int evtType = (int) (Math.random() * Constants.possibleEvent.length);
		RoundTableDataExample rData = new RoundTableDataExample(sdf.format(c
				.getTime()), (int) (Math.random() * 10 - 1), evtType);

		return rData;

	}

}
