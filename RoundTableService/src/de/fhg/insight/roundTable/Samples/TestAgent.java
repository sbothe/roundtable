package de.fhg.insight.roundTable.Samples;

import java.util.LinkedList;
import java.util.NoSuchElementException;

import de.fhg.insight.roundTable.IRoundTableAgent;
import de.fhg.insight.roundTable.RoundTable;
import de.fhg.insight.roundTable.RoundTableData;

/**
 * 
 * @author abablok
 * @author sbothe
 * 
 */
public class TestAgent implements IRoundTableAgent {

	private boolean callingAgent = false;
	private final LinkedList<RoundTableDataExample> data = new LinkedList<RoundTableDataExample>(); // muss
	// dynamisches
	// Array
	// sein,
	// oder
	// liste.
	// private TreeMap<Data,Number> relevant_data = new TreeMap<Data,Number>();
	private final LinkedList<RoundTableDataExample> relevant_data = new LinkedList<RoundTableDataExample>();
	RoundTable participating_at;
	private final String agentID; // description of the sensor we are dealing
									// with

	public TestAgent() {
		// TODO: find a right way to determine an ID..
		this.agentID = "Agent_" + (int) (Math.random() * 1000);

	}

	public TestAgent(RoundTableDataExample sensorData, String agentID) {
		this.agentID = agentID;
		this.data.add(sensorData);
	}

	@Override
	public String getInfo() {
		return agentID;
	}

	@Override
	public void joinTable(RoundTable t) {
		this.participating_at = t;
		participating_at.addAgent(this);
		System.out.println("Agent for " + this.getInfo() + " joined");
	}

	@Override
	public void leaveTable() {
		participating_at.deleteAgent(this);
	}

	@Override
	public void callTable(RoundTableData someData, RoundTable t) {
		try {
			t.setIssue(someData);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.callingAgent = true;
		this.joinTable(t);
	}

	public RoundTableDataExample mostRelevant() {
		RoundTableDataExample i = null;
		double rel = 200;
		for (RoundTableDataExample d : relevant_data) {
			if (d.get_relevance() <= rel) {
				i = d;
				rel = i.get_relevance();
			}
		}

		return i;
	}

	// Collect Information on the given Data, i.e. search for related Data
	// (called by the round table)
	@Override
	public void collectInformation() {
		double boundary = 4;
		double d_date;
		double d_event;
		double d_loc;
		if (!this.callingAgent) {
			for (RoundTableDataExample d : data) {

				if (participating_at.getIssue().getEvent() < 0) {
					d_event = 0;
					boundary--;
				} else {
					d_event = d.getEvent()
							- participating_at.getIssue().getEvent();
				}

				if (participating_at.getIssue().getLocation() < 0) {
					d_loc = 0;
					boundary--;
				} else {
					d_loc = d.getLoc()
							- participating_at.getIssue().getLocation();
				}
				long t = d.getDate().getTime().getTime();
				long x = participating_at.getIssue().getDate().getTime()
						.getTime();
				d_date = (t - x) / (60. * 60 * 1000);
				// System.out.println(d_date);
				d.change_relevance(euklidian(d_event, d_loc, d_date));

				// The agent provide information based on a distance
				// System.out.println("Relevanz: "+d.get_relevance());
				if (d.get_relevance() < boundary) {
					// relevant_data.put(d,d.get_relevance());
					this.relevant_data.add(d);
				}

			}
		}

	}

	// Put Data for discussion on round table
	@Override
	public void cardsOn() {
		if (!this.callingAgent) {
			System.out.println(this.getInfo() + " says "
					+ this.getRelevantInf());
			participating_at.addData(this, this.mostRelevant());
		}
	}

	@Override
	public void addData(RoundTableDataExample d) {
		data.add(d);
	}

	public String getRelevantInf() {
		try {
			// return this.relevant_data.firstKey().getDataInf();
			return this.mostRelevant().toString();

		} catch (NoSuchElementException e) {
			return "\"nothing to say\"";
		} catch (IndexOutOfBoundsException e) {
			return "\"nothing to say\"";
		} catch (NullPointerException e) {
			return "\"nothing to say\"";
		}
		// return "";
	}

	@Deprecated
	public double euklidian(double a, double b, double c) {
		return Math.sqrt(a * a + b * b + c * c);
	}

	@Deprecated
	public double euklidian(double a, double b) {
		return Math.sqrt(a * a + b * b);
	}

}