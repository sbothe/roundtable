package de.fhg.insight.roundTable.Samples;

//import java.text.SimpleDateFormat;
//import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 
 * @author abablok
 * @author sbothe
 * 
 */
public class RoundTableDataExample extends
		de.fhg.insight.roundTable.RoundTableData {

	private final static int MAXACCURACY = 1000;
	private final int location;
	private int event;
	private int range_date = 0;
	private int range_loc = 0;
	private int range_event = 0;
	private double relevance;
	private double accuracy;
	private final Calendar c = new GregorianCalendar(1900, Calendar.JANUARY, 1);

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public RoundTableDataExample(String date, int location, int event) {
		accuracy = 0;
		System.out.println("New data constructed");
		if (!(date == null)) {
			try {
				this.c.setTime(sdf.parse(date));
				System.out.println("Set date to:"
						+ sdf.format(this.c.getTime()));
			} catch (ParseException e) {
				System.out
						.println("Parsing Error. Did you choose the right Dateformat?");
			}
		} else {
			System.out.println("No Date chosen");
		}
		// System.out.println("Locationnumber: " + location);
		this.location = location;
		if (!(this.location < 0)) {
			System.out.println("Set location to:"
					+ Constants.possibleLoc[location]);
		} else {
			System.out.println("No location chosen");
		}
		// System.out.println("Eventnumber: " + event);
		this.event = event;
		if (!(this.event < 0)) {
			System.out
					.println("Set event to:" + Constants.possibleEvent[event]);
		} else {
			System.out.println("No event chosen");
		}
	}

	public RoundTableDataExample(String date, int location, int event,
			int range_date, int range_loc, int range_event) {
		this(date, location, event);
		if (event < 0) {
			range_event = MAXACCURACY;
		}
		if (location < 0) {
			range_loc = MAXACCURACY;
		}
		this.range_date = range_date;
		this.range_loc = range_loc;
		this.range_event = range_event;
		this.accuracy = range_date + range_loc + range_event;

	}

	void change_relevance(double new_r) {
		this.relevance = new_r;
	}

	double get_relevance() {
		return this.relevance;
	}

	@Override
	public double getAccuracy() {
		return this.accuracy;
	}

	@Override
	public void setEvent(int e) {
		this.event = e;
	}

	int get_range_date() {
		return range_date;
	}

	int get_range_loc() {
		return range_loc;
	}

	@Override
	public int getRange_event() {
		return range_event;
	}

	public static RoundTableDataExample copy(RoundTableDataExample i) {
		RoundTableDataExample ret = new RoundTableDataExample(i.getDateStr(),
				i.getLoc(), i.getEvent(), i.get_range_date(),
				i.get_range_loc(), i.getRange_event());
		return ret;
	}

	@Override
	public Calendar getDate() {
		return this.c;
	}

	String getDateStr() {
		return sdf.format(this.c.getTime());
	}

	@Override
	public int getEvent() {
		return this.event;
	}

	int getLoc() {
		return this.location;
	}

	@Override
	public String toString() {
		String event = "No Event";
		String loc = "No Loc";
		try {
			event = Constants.possibleEvent[this.getEvent()];
		} catch (IndexOutOfBoundsException e) {
		}

		try {
			loc = Constants.possibleLoc[this.getLoc()];
		} catch (IndexOutOfBoundsException e) {
		}

		return this.getDateStr() + " " + event + " " + loc;
	}

}