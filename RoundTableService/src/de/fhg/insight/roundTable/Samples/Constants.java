package de.fhg.insight.roundTable.Samples;

public final class Constants {
	public static final String[] possibleLoc = { "Koblenz", "Bad Honnef",
			"Koenigswinter", "Schloss Birlinghoven", "Bonn", "Koeln",
			"Dortmund", "Hamburg", "Berlin" };// 0-8
	public static final String[] possibleEvent = { "Ueberfall",
			"Brandstiftung", "Brand", "Explosion", "Nuklear" };// 0-4
}
