package de.fhg.insight.roundTable.Bayes;

import java.io.Serializable;
import java.util.Comparator;

import org.joda.time.DateTime;


public class BayesEvent implements Comparable<BayesEvent>, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -399155552273099402L;
	Area area;
	TimeInterval interval;
	BayesEventType type;
	public BayesEvent(Area area, TimeInterval interval,
			BayesEventType type) {
		super();
		this.area = area;
		this.interval = interval;
		this.type = type;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public TimeInterval getInterval() {
		return interval;
	}
	public void setInterval(TimeInterval interval) {
		this.interval = interval;
	}
	public BayesEvent(Area area, TimeInterval interval) {
		super();
		this.area = area;
		this.interval = interval;
	}
	public BayesEventType getType() {
		return type;
	}
	public void setType(BayesEventType type) {
		this.type = type;
	}
	public String toString(){
		return this.type + "\n\t Location: " + this.area + "\n\t Time: " + this.interval + "\n" ;
	}
	
	public static Comparator<BayesEvent> BayesEventStartTimeComparator 
	= new Comparator<BayesEvent>() {

		public int compare(BayesEvent event1, BayesEvent event2) {

			DateTime time1 = event1.getInterval().getStart();
			DateTime time2 = event2.getInterval().getStart();

			//ascending order
			return time1.compareTo(time2);

			//descending order
			//return fruitName2.compareTo(fruitName1);
		}

	};
	@Override
	public int compareTo(BayesEvent o) {
		DateTime time1 = this.getInterval().getStart();
		DateTime time2 = o.getInterval().getStart();
		return time1.compareTo(time2);
	}
}
