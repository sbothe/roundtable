package de.fhg.insight.roundTable.Bayes;

import java.util.LinkedList;
import java.util.Vector;

import org.joda.time.ReadableInstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesAgent;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.IISA2RTemitter;


/**
*
* @author fschnitzler
*
*/
public abstract class BaseBayesAgent<TroundTable> implements IBayesAgent<TroundTable>{
	final static Logger log = LoggerFactory.getLogger(BaseBayesAgent.class);

	IISA2RTemitter<TroundTable> ISA2RTemitter = null;
	//private final LinkedList<BayesRoundTableData> data = new LinkedList<BayesRoundTableData>(); // muss
	//private final LinkedList<BayesRoundTableData> relevant_data = new LinkedList<BayesRoundTableData>();
	protected Vector<TroundTable> participating_at = new Vector<TroundTable>();
	private Vector<Area> participating_atAreas = new Vector<Area>();
	private Vector<ReadableInstant> participating_atStartTime = new Vector<ReadableInstant>();
	private String agentID; // description of the sensor we are dealing with
	
	// Constructors
	public BaseBayesAgent() {
		// TODO: find a right way to determine an ID..
		this.agentID = "Agent_" + (int) (Math.random() * 1000);

	}
	public BaseBayesAgent(String ID) {
		this.agentID = ID;
	}

//	public BaseBayesAgent(BayesRoundTableData sensorData, String agentID) {
//		this.agentID = agentID;
//		this.data.add(sensorData);
//	}

	@Override
	public String getID() {
		return agentID;
	}
	@Override
	public void setId(String agentID) {
		this.agentID = agentID;
	}
	@Override
	public void joinTable(TroundTable t, Area area, ReadableInstant startTime){
		this.participating_at.add(t);
		this.participating_atAreas.add(area);
		this.participating_atStartTime.add(startTime);
		//participating_at.addAgent(this);
		log.debug("Agent {} joined table {}", this.getID(), t);
	}
	@Override
	public void updateArea(TroundTable bayesRoundTable, Area searchArea){
		int tableIndex = this.getInternalRoundTableIndex(bayesRoundTable);
		this.participating_atAreas.set(tableIndex,searchArea);
	}


	@Override
	// return the index of the left roundtable in the internal memory of the agent
	public int leaveTable(TroundTable t) {
		int index = this.getInternalRoundTableIndex(t);
		participating_at.remove(index);
		participating_atAreas.remove(index);
		participating_atStartTime.remove(index);
		log.debug("Agent {} left table {}", this.getID(), t);
		return index;
	}
	
	public int getTableNumber(){
		return this.participating_at.size();
	}

	// F: No clue what this was used for
//	//@Override
//	public void callTable(IRoundTableData iData, TroundTable it) {
//		BayesRoundTableData someData = (BayesRoundTableData) iData ;
//		BayesRoundTable t = (BayesRoundTable) it;
//		try {
//			t.setIssue(someData);
//		} catch (CloneNotSupportedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		this.callingAgent = true;
//		this.joinTable(t);
//	}

//	public BayesRoundTableData mostRelevant() {
////		ProbabilisticRoundTableData i = null;
////		double rel = 200;
////		for (ProbabilisticRoundTableData d : relevant_data) {
////			if (d.get_relevance() <= rel) {
////				i = d;
////				rel = i.get_relevance();
////			}
////		}
////
////		return i;
//		return null;
//	}

	// Collect Information on the given Data, i.e. search for related Data
	//@Override
	// make this abstract
//	public void collectInformation(BayesEvent query) {
//		double boundary = 4;
//		double d_date;
//		double d_event;
//		double d_loc;
//		if (!this.callingAgent) {
//			for (ProbabilisticRoundTableData d : data) {
//
//				if (participating_at.getIssue().getEvent() < 0) {
//					d_event = 0;
//					boundary--;
//				} else {
//					d_event = d.getEvent()
//							- participating_at.getIssue().getEvent();
//				}
//
//				if (participating_at.getIssue().getLocation() < 0) {
//					d_loc = 0;
//					boundary--;
//				} else {
//					d_loc = d.getLoc()
//							- participating_at.getIssue().getLocation();
//				}
//				long t = d.getDate().getTime().getTime();
//				long x = participating_at.getIssue().getDate().getTime()
//						.getTime();
//				d_date = (t - x) / (60. * 60 * 1000);
//				// System.out.println(d_date);
//				d.change_relevance(euklidian(d_event, d_loc, d_date));
//
//				// System.out.println("Relevanz: "+d.get_relevance());
//				if (d.get_relevance() < boundary) {
//					// relevant_data.put(d,d.get_relevance());
//					this.relevant_data.add(d);
//				}
//
//			}
//		}

//	}

	// Put Data for discussion on round table
	//@Override
//	public void cardsOn() {
//		if (!this.callingAgent) {
//			System.out.println(this.getInfo() + " says "
//					+ this.getRelevantInf());
//			ProbabilisticEventData data = new ProbabilisticEventData();
//			
//			data.put(new BayesEventType("storm"),0.1);
//			data.put(new BayesEventType("flood"),0.4);
//			data.put(new BayesEventType("no internet"),0.9);
//			int randomLocation = (int) ((Math.random() * 9) - 2);
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			BayesRoundTableData event = new BayesRoundTableData(sdf, randomLocation, data);
//			
//			System.out.println(event);
//			// TODO: set data here, include probabilities!!
//			//participating_at.addData(this, this.mostRelevant());
//			participating_at.addData(this, event);
//		}
//	}

//	public String getRelevantInf() {
//		try {
//			// return this.relevant_data.firstKey().getDataInf();
//			return this.mostRelevant().toString();
//
//		} catch (NoSuchElementException e) {
//			return "\"nothing to say\"";
//		} catch (IndexOutOfBoundsException e) {
//			return "\"nothing to say\"";
//		} catch (NullPointerException e) {
//			return "\"nothing to say\"";
//		}
//		// return "";
//	}

	//TODO: implement this in a second phase
	//@Override
	//rate the importance of an event
	// public void rate_data(BayesRoundTableData issue, TroundTable d) {
		/*
		 * double min=0; int loc; int event; int i,j; loc=issue.getLoc();
		 * event=issue.getEvent();
		 * for(i=event-(int)issue.get_range_event()/2;i<=
		 * event+(int)issue.get_range_event()/2;i++){ for(j=loc-(int)
		 * issue.get_range_loc()/2;j<=loc+(int)issue.get_range_loc()/2;j++){
		 *
		 * if(min<euklidian(i,j)){ } } }
		 */
	// }

	@Override
	public void SignalData(BayesRoundTableData anomalousData) {
		 this.getISA2RTemitter().newAnomaly(anomalousData, this);
	}

	@Override
	public void addData(BayesRoundTableData data, TroundTable table) {
		//TODO: specialize
		this.getISA2RTemitter().addData(data, table, this);	
	}


	public IISA2RTemitter<TroundTable> getISA2RTemitter() {
		return ISA2RTemitter;
	}

	public void setISA2RTemitter(IISA2RTemitter<TroundTable> iSA2RTemitter) {
		ISA2RTemitter = iSA2RTemitter;
	}

	public int getInternalRoundTableIndex(TroundTable roundTableID){
		return this.participating_at.indexOf(roundTableID);
	}
}