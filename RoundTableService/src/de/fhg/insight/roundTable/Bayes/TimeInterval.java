package de.fhg.insight.roundTable.Bayes;

import org.joda.time.Chronology;
import org.joda.time.ReadableDuration;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePeriod;

public class TimeInterval extends org.joda.time.MutableInterval{

	public TimeInterval() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(long arg0, long arg1, Chronology arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(long arg0, long arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(Object arg0, Chronology arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(Object arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(ReadableDuration arg0, ReadableInstant arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(ReadableInstant arg0, ReadableDuration arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(ReadableInstant arg0, ReadableInstant arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(ReadableInstant arg0, ReadablePeriod arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public TimeInterval(ReadablePeriod arg0, ReadableInstant arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6303608932420459385L;
	
}
