package de.fhg.insight.roundTable.Bayes.Model;

import myRandom.MyRandom;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import org.joda.time.ReadableInstant;

import de.fhg.insight.roundTable.Bayes.BayesEventType;
import de.fhg.insight.roundTable.Bayes.OntologyDistribution;

import org.apache.commons.math3.distribution.NormalDistribution;
//This class generates a sample based on active events normality as a single Gaussian and uses 
// P(x|model) and 1-P(x|model) as the probability to build to RoundTableData
public class ModelIndepGaussian implements IModelIndep {
	NormalDistribution distribution = null;
	//TODO: this is badly done, the noise is actually generated outse, in BayesAgentCellXXXXX
	NormalDistribution noiseDistribution = new NormalDistribution(0.0,3.0);
	
	
	//TODO: fix generator
	public ModelIndepGaussian(double mean, double sd) {
		super();
		distribution = new NormalDistribution(mean,sd);
	}

	@Override
	public OntologyDistribution computeOntologyDistribution(Double data,
			ReadableInstant time) {
		//TODO maybe make a test on pValue.
//		double cumulProba = this.distribution.cumulativeProbability(data);
//		if (cumulProba>0.5)
//			cumulProba = 1-cumulProba;
//		double pValue = 2*cumulProba;
		OntologyDistribution ontologyDistribution= new OntologyDistribution(BayesEventType.class);
		ontologyDistribution.put(BayesEventType.NORMAL, this.distribution.density(data));
		ontologyDistribution.put(BayesEventType.ANOMALY, this.noiseDistribution.density(data));
		return ontologyDistribution;
	}

	@Override
	public Double sample(ReadableInstant time) {
		// TODO Auto-generated method stub
		return distribution.sample();
	}

	@Override
	//TODO: move noise here here.
	public Double sample(ReadableInstant time, OntologyDistribution activeEvents) {
		return this.sample(time);
	}

	@Override
	public boolean isAnomaly(Double data, ReadableInstant time) {
		double cumulProba = this.distribution.cumulativeProbability(data);
		if (cumulProba>0.5)
			cumulProba = 1-cumulProba;
		double pValue = 2*cumulProba;		
		if (pValue<0.001)
			return true;
		else
			return false;
	}

	public static IModelIndep createFromJSON(String input) {
		//get the values
		JSONObject obj= (JSONObject) JSONValue.parse(input);
		Double meanValue= (Double) obj.get("mean");
		Double stdValue= (Double) obj.get("std");
		Double noiseMeanValue= (Double) obj.get("noiseMean");
		Double noiseStdValue= (Double) obj.get("noiseStd");
		//recreate model
		ModelIndepGaussian output = new ModelIndepGaussian(meanValue,stdValue);
		output.noiseDistribution = new NormalDistribution(noiseMeanValue,noiseStdValue);
		return output;
	}

	@Override
	public String toJSONString() {
		return "{\"Type\":\"ModelIndepGaussian\",\"mean\":"+ this.distribution.getMean() +",\"std\":"+ this.distribution.getStandardDeviation() 
				+ ",\"noiseMean\":"+ this.noiseDistribution.getMean() +",\"noiseStd\":"+ this.noiseDistribution.getStandardDeviation() +"}";
	}
	
}
