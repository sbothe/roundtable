package de.fhg.insight.roundTable.Bayes.Model;

import java.util.Map.Entry;

import net.minidev.json.JSONArray;

import org.joda.time.ReadableInstant;

import de.fhg.insight.roundTable.Bayes.BayesEventType;
import de.fhg.insight.roundTable.Bayes.OntologyDistribution;

import org.apache.commons.math3.distribution.NormalDistribution;
//This class generates a sample based on active events
//this class generates a number between 0 and 9999. Each digit represents 
//the probability of the data based on an event. 
//TODO: get free of that pesky double as the data type
//TODO: this IndepEventType is completely hardcoded. need to introduce modularity.
public class ModelIndepEventType implements IModelIndep {
	NormalDistribution distribution = null;
	
	//TODO: add distribution
	public ModelIndepEventType() {
		super();
	}

	@Override
	public OntologyDistribution computeOntologyDistribution(Double data,
			ReadableInstant time) {
		OntologyDistribution ontologyDistribution= new OntologyDistribution(BayesEventType.class);
		//TODO: write better distribution
		Double currprobaTimes10 = data % 10;
		data = (data - currprobaTimes10)/10; // prepare for next one
		ontologyDistribution.put(BayesEventType.FLOOD, currprobaTimes10/10);
		currprobaTimes10 = data % 10;
		data = (data - currprobaTimes10)/10; // prepare for next one
		ontologyDistribution.put(BayesEventType.FIRE, currprobaTimes10/10);
		currprobaTimes10 = data % 10;
		data = (data - currprobaTimes10)/10; // prepare for next one
		ontologyDistribution.put(BayesEventType.SNOW, currprobaTimes10/10);
		currprobaTimes10 = data % 10;
		data = (data - currprobaTimes10)/10; // prepare for next one
		ontologyDistribution.put(BayesEventType.TRAFFIC_JAM, currprobaTimes10/10);
		//normal case
		ontologyDistribution.put(BayesEventType.NORMAL, 2.0/10);
		//anomaly
		ontologyDistribution.put(BayesEventType.ANOMALY, Math.max(
				Math.max(ontologyDistribution.get(BayesEventType.FLOOD),
						ontologyDistribution.get(BayesEventType.FIRE)),
				Math.max(ontologyDistribution.get(BayesEventType.SNOW),
						ontologyDistribution.get(BayesEventType.TRAFFIC_JAM))
				));

		return ontologyDistribution;
	}

	@Override
	//not supposed to be called.
	//TODO: remove
	public Double sample(ReadableInstant time) {
		return null;
	}
	
	// OntologyDistribution : number of active events of each type in that cell
	@Override
	public Double sample(ReadableInstant time, OntologyDistribution activeEvents) {
		Double data = 1111.0;
		if (activeEvents!=null){
			for(OntologyDistribution.Entry<BayesEventType, Double> entry : activeEvents.entrySet()){
				// if there is no event
				if (entry.getKey() == null || entry.getValue() == 0.0){
					//				switch (entry.getKey() ) {
					//				case FLOOD:
					//					data=data+1;
					//					break;
					//				case FIRE:
					//					data=data+10;
					//					break;
					//				case SNOW:
					//					data=data+100;
					//					break;
					//				case TRAFFIC_JAM:
					//					data=data+1000;
					//					break;
					//				default:
					//					break;
					//				}
				}
				else { //there is an event, woohoo
					switch (entry.getKey()) {
					case FLOOD:
						data=data+7;
						break;
					case FIRE:
						data=data+70;
						break;
					case SNOW:
						data=data+700;
						break;
					case TRAFFIC_JAM:
						data=data+7000;
						break;
					default:
						break;
					}
				}
			}	
		}

		return data;
	}

	@Override
	//TODO: do better than that
	public boolean isAnomaly(Double data, ReadableInstant time) {
		return false;
	}

	public static IModelIndep createFromJSON(String input) {
		
		return new ModelIndepEventType();
	}

	@Override
	public String toJSONString() {
		return "{\"Type\":\"ModelIndepEventType\"}";
	}
	
}
