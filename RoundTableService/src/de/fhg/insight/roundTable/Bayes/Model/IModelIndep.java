package de.fhg.insight.roundTable.Bayes.Model;

import net.minidev.json.JSONAware;

import org.joda.time.ReadableInstant;

import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.OntologyDistribution;

/*
 * An IModelIndep is a model able to rate data according to a model and build the probabilistic part 
 * of the RoundTableData
 */
public interface IModelIndep extends JSONAware{
	/*
	 * This function provides a distribution over a subset of the ontology of events. 
	 * This subset doesn't have to contain the root, but its union with the root has to be connected.
	 */
	public OntologyDistribution computeOntologyDistribution(Double data, ReadableInstant time);
	/*
	 * This function evaluates whether @data obtained at a given @time is an anomaly.
	 */
	public boolean isAnomaly(Double data, ReadableInstant time);

	//TODO: better separate, the following functions are used for simulation
	public Double sample(ReadableInstant time);
	public Double sample(ReadableInstant time, OntologyDistribution activeEvents);
}
