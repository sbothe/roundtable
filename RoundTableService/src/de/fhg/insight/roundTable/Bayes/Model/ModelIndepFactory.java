package de.fhg.insight.roundTable.Bayes.Model;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

public class ModelIndepFactory {
	public static IModelIndep createFromJSON(String input){
		JSONObject obj= (JSONObject) JSONValue.parse(input);
		String type= (String) obj.get("Type");
		IModelIndep newModelIndep = null;
		switch (type){
		case "ModelIndepGaussian":
			newModelIndep = ModelIndepGaussian.createFromJSON(input);
			break;
		case "ModelIndepEventType":
			newModelIndep = ModelIndepEventType.createFromJSON(input);
			break;
		}
		return newModelIndep;
	}
}
