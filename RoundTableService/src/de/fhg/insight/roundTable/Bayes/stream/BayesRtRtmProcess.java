package de.fhg.insight.roundTable.Bayes.stream;

import java.io.Serializable;
import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.ReadableInstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesRoundTable;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableManager;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableStatus;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RT2ISAemitter;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RToutputEmitter;
import de.fhg.insight.roundTable.Bayes.stream.messageType.rtToISA;
import stream.Data;
import stream.ProcessContext;
import stream.StatefulProcessor;
import stream.data.DataFactory;
import stream.io.Sink;

//TODO encapsulate the BayesRoundTableManager instead of deriving from it.
public class BayesRtRtmProcess implements StatefulProcessor, RT2ISAemitter<String>, RToutputEmitter{
	static Logger log = LoggerFactory.getLogger(BayesRtRtmProcess.class);
	
	String keyStreamType = "@StreamType";		//key to identify a stream
	String keyMessageType = "@MessageType";		//key to identify a message sent on a stream
	String keyRoundTableID = "roundTableID";
	String keyArea = "area";
	String keyStartTime = "startTime";
	String keyRoundTableData = "roundTableData";
	String keyIntelligentSensorAgentID = "intelligentSensorAgentID";
	String keyEventDetectedTime = "eventDetectedTime";
	String keyRoundTableStatus = "roundTableStatus";
	//BayesRoundTableManager<String> rountTableManager = null;
	
	int maxNumberRT = 0;
	int minimumDurationOfEvent = 300000; //default: 5 minutes (in milliseconds)
	int timeWithoutSupportingDataBeforeClosing = 300000; //default: 5 minutes (in milliseconds)
	
	Sink[] list_isa;
	Sink output;
	
	BayesRoundTableManager<String> roundTableManager = null;
	
	HashMap<String,Sink> sinkMap = new HashMap<String,Sink>();
	
	/*** incoming communication ***/
	@Override
	public Data process(Data item) {
		// get stream type
		Serializable valueStreamType = item.get( keyStreamType ); // item.get( key )
		String streamType = valueStreamType.toString();
		// switch based on type
		switch (streamType.toLowerCase()) {
		case "anomaly":
			this.processAnomaly(item);
			break;
		case "answer":
			this.processAnswer(item);
			break;
		default: 
			log.info("Error: unknown event type {}", streamType);
			break;	
		}
		return item;
	}
	private void processAnomaly(Data item) {
		log.debug("receiving anomaly {}",item);
		Serializable agentValue = item.get(keyIntelligentSensorAgentID);
		String agentID = agentValue.toString();
		Serializable roundTableDataValue = item.get(keyRoundTableData);
		//TODO not safe
		BayesRoundTableData anomaly = (BayesRoundTableData) roundTableDataValue;
		
		this.roundTableManager.newAnomaly(anomaly, agentID);
	}
	
	private void processAnswer(Data item) {
		log.debug("receiving answer {}",item);
		//get back data
		Serializable dataValue = item.get(keyRoundTableData);
		BayesRoundTableData data = (BayesRoundTableData) dataValue;
		Serializable tableValue = item.get(keyRoundTableID);
		Long table = Long.parseLong(tableValue.toString());
		Serializable agentValue = item.get(keyIntelligentSensorAgentID);
		String agentID = agentValue.toString();
		
		// give to table
		this.roundTableManager.giveAnswerToRT(data, table, agentID);
	}
	/*** life cycle ***/
	@Override
	public void init(ProcessContext context) throws Exception {		
		this.resetState();
	}
	@Override
	public void resetState() throws Exception {
		this.roundTableManager = new BayesRoundTableManager<String>(this);
		this.roundTableManager.setMaxNumberRT(this.getMaxNumberRT());
		this.roundTableManager.setMinimumDurationOfEvent(minimumDurationOfEvent);
		this.roundTableManager.setTimeWithoutSupportingDataBeforeClosing(timeWithoutSupportingDataBeforeClosing);
		this.roundTableManager.setOutputEmitter(this);
		
		for(String agentID : this.sinkMap.keySet())
			this.roundTableManager.registerAgent(agentID);
	}
	@Override
	public void finish() throws Exception {
		// TODO Auto-generated method stub
		
	}
	/*** outgoing communication RT2ISAemitter ***/

	@Override
	public void tellAgentToUpdateSearch(String participatingAgent,
			IBayesRoundTable<String> bayesRoundTable, Area searchArea2) {
		log.debug("telling {} to update search for table {} to {}",participatingAgent,bayesRoundTable,searchArea2);

		Data item = DataFactory.create();
		item.put(keyMessageType, rtToISA.UPDATE);
		item.put(keyRoundTableID, bayesRoundTable.getId());
		item.put(keyArea, searchArea2);
		this.tellAgent(item, participatingAgent);
	}
	@Override
	public void tellAgentToJoin(String a, BayesRoundTable<String> table,
			Area area, DateTime startTime) {
		log.debug("telling {} to join table {}",a,table);
		Data item = DataFactory.create();
		item.put(keyMessageType, rtToISA.JOIN);
		
		item.put(keyRoundTableID, table.getId());
		item.put(keyArea, area);
		item.put(keyStartTime, startTime);
		this.tellAgent(item, a);
	}
	@Override
	public void tellAgentToLeave(String a,
			IBayesRoundTable<String> bayesRoundTable) {
		log.debug("telling {} to leave table {}",a,bayesRoundTable);
		Data item = DataFactory.create();
		item.put(keyMessageType, rtToISA.LEAVE);
		item.put(keyRoundTableID, bayesRoundTable.getId());
		this.tellAgent(item, a);
	}
	
	public void tellAgent(Data item, String a){
		item.put(keyStreamType, "query");
		try {
			this.sinkMap.get(a).write(item);
		} catch (Exception e) {
			log.error("Failed to send query item {} to ISA sink '{}': {}", item,
					this.sinkMap.get(a), e.getMessage());
			e.printStackTrace();
		}  
	}
	
	/*** outgoing communication of detected events ***/
	@Override
	public void emitReport(Long roundTableID, Instant currentTime,
			BayesRoundTableData event, BayesRoundTableStatus roundTableStatus) {
		Data item = DataFactory.create();	
		item.put(keyRoundTableID, roundTableID);
		item.put(keyEventDetectedTime, currentTime);
		item.put(keyRoundTableStatus,roundTableStatus);
		item.put(keyRoundTableData, event);
		try {
			this.output.write(item);
		} catch (Exception e) {
			log.error("Failed to output  {}", item);
			e.printStackTrace();
		}
	}
	
	/*** getters / setters ***/

	public String getKeyStreamType() {
		return keyStreamType;
	}
	public void setKeyStreamType(String keyStreamType) {
		this.keyStreamType = keyStreamType;
	}
	public String getKeyMessageType() {
		return keyMessageType;
	}
	public void setKeyMessageType(String keyMessageType) {
		this.keyMessageType = keyMessageType;
	}
	public String getKeyRoundTableID() {
		return keyRoundTableID;
	}
	public void setKeyRoundTableID(String keyRoundTableID) {
		this.keyRoundTableID = keyRoundTableID;
	}
	public String getKeyArea() {
		return keyArea;
	}
	public void setKeyArea(String keyArea) {
		this.keyArea = keyArea;
	}
	public String getKeyStartTime() {
		return keyStartTime;
	}
	public void setKeyStartTime(String keyStartTime) {
		this.keyStartTime = keyStartTime;
	}
	public String getKeyRoundTableData() {
		return keyRoundTableData;
	}
	public void setKeyRoundTableData(String keyRoundTableData) {
		this.keyRoundTableData = keyRoundTableData;
	}
	public String getKeyIntelligentSensorAgentID() {
		return keyIntelligentSensorAgentID;
	}
	public void setKeyIntelligentSensorAgentID(String keyIntelligentSensorAgentID) {
		this.keyIntelligentSensorAgentID = keyIntelligentSensorAgentID;
	}
	public Sink[] getOutput2() {
		return list_isa;
	}
	public void setOutput2(Sink[] output2) {
		this.list_isa = output2;
		for(int indSink = 0; indSink<output2.length; indSink++)
			this.sinkMap.put(output2[indSink].getId(), output2[indSink]);
	}
	
	public Sink getOutput() {
		return output;
	}
	public void setOutput(Sink output) {
		this.output = output;
	}
	public RT2ISAemitter<String> clone(){
		return this;
	}
	public int getMaxNumberRT() {
		return maxNumberRT;
	}
	public void setMaxNumberRT(int maxNumberRT) {
		if (this.roundTableManager != null)
				this.roundTableManager.setMaxNumberRT(maxNumberRT);
		this.maxNumberRT = maxNumberRT;
	}
	public int getMinimumDurationOfEvent() {
		return minimumDurationOfEvent;
	}
	public void setMinimumDurationOfEvent(int minimumDurationOfEvent) {
		if (this.roundTableManager != null)
			this.roundTableManager.setMinimumDurationOfEvent(minimumDurationOfEvent);
		this.minimumDurationOfEvent = minimumDurationOfEvent;
	}
	public int getTimeWithoutSupportingDataBeforeClosing() {
		return timeWithoutSupportingDataBeforeClosing;
	}
	public void setTimeWithoutSupportingDataBeforeClosing(
			int timeWithoutSupportingDataBeforeClosing) {
		if (this.roundTableManager != null)
			this.roundTableManager.setTimeWithoutSupportingDataBeforeClosing(timeWithoutSupportingDataBeforeClosing);
		this.timeWithoutSupportingDataBeforeClosing = timeWithoutSupportingDataBeforeClosing;
	}

}
