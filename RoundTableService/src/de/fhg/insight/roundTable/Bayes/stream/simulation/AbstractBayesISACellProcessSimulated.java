package de.fhg.insight.roundTable.Bayes.stream.simulation;

import java.io.Serializable;

import org.joda.time.Duration;
import org.joda.time.Instant;

import stream.Data;
import stream.ProcessContext;
import stream.util.parser.TimeParser;
import de.fhg.insight.roundTable.Bayes.BayesAgentCell;
import de.fhg.insight.roundTable.Bayes.simulation.BayesAgentCellSimulated;
import de.fhg.insight.roundTable.Bayes.stream.AbstractBayesISACellProcess;
import de.fhg.insight.roundTable.Bayes.stream.simulation.services.SimulatorServices;

public abstract class AbstractBayesISACellProcessSimulated<TagentType extends BayesAgentCellSimulated<Long> > extends
		AbstractBayesISACellProcess<BayesAgentCellSimulated<Long>> {
	String keySimulationTime = "simulationTime";
	SimulatorServices simulatorService = null;
	//Duration twitterSamplingPeriod = null;//new Duration(systemTimeInterval.toDurationMillis()/200);
	Duration samplingPeriod;
	
	@Override
	protected void processSimul(Data item) {
		Serializable simulationCurrentTimeValue = item.get(keySimulationTime);
		Instant simulationCurrentTime = (Instant) simulationCurrentTimeValue;
			this.getAgent().simulateAndAddData(simulationCurrentTime);
	}

	@Override
	public void configureAgent(ProcessContext context) {
		super.configureAgent(context);
		// add events from the simulator
		this.getAgent().setMyEvents(simulatorService.getSimulatedEvents());
		
		// add sampling time information
		this.getAgent().setLastSampledTime(simulatorService.getSimulationStartTime());
		this.getAgent().setSamplingPeriod(samplingPeriod);
	}

	public String getKeySimulationTime() {
		return keySimulationTime;
	}

	public void setKeySimulationTime(String keySimulationTime) {
		this.keySimulationTime = keySimulationTime;
	}

	public SimulatorServices getSimulatorService() {
		return simulatorService;
	}

	public void setSimulatorService(SimulatorServices simulatorService) {
		this.simulatorService = simulatorService;
	}

	public Duration getSamplingPeriod() {
		return samplingPeriod;
	}

	public void setSamplingPeriod(String samplingPeriod) {
		Long periodMilli;
		try {
			periodMilli = TimeParser.parseTime(samplingPeriod);
		} catch (Exception e) {
			periodMilli = -1L;
			throw new RuntimeException("Invalid time string '" + samplingPeriod
					+ "': " + e.getMessage());
		}
		this.samplingPeriod = new Duration(periodMilli);
	}	
}
