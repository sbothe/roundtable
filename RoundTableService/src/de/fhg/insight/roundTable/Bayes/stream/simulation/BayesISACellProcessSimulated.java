package de.fhg.insight.roundTable.Bayes.stream.simulation;

import de.fhg.insight.roundTable.Bayes.simulation.BayesAgentCellSimulated;

public class BayesISACellProcessSimulated extends
		AbstractBayesISACellProcessSimulated<BayesAgentCellSimulated<Long>>{

	@Override
	protected BayesAgentCellSimulated<Long> constructAgent() {
		//TODO: include this in the model
		BayesAgentCellSimulated<Long> tmp = new BayesAgentCellSimulated<Long>();
		tmp.setNoiseDistribution(0.0, 2.0);
		return tmp;
	}
}
