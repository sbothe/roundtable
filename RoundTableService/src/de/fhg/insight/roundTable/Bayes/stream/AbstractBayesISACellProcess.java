package de.fhg.insight.roundTable.Bayes.stream;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONValue;

import org.joda.time.ReadableInstant;

import stream.Data;
import stream.ProcessContext;
import stream.expressions.ExpressionResolver;
import stream.io.JSONStream;
import stream.io.SourceURL;
import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesAgentCell;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesAgent;
import de.fhg.insight.roundTable.Bayes.Model.IModelIndep;
import de.fhg.insight.roundTable.Bayes.Model.ModelIndepFactory;

public abstract class AbstractBayesISACellProcess<TagentType extends BayesAgentCell<Long> > extends AbstractBayesISAprocess<TagentType> {
	String urlStringISASetup; 
	
	@Override
	protected void processData(Data item) {
		//get data
		Serializable newDataValue = item.get("data");
		Vector<Double> newData = (Vector<Double>) newDataValue;
		//get time
		Serializable timeValue = item.get("instant");
		ReadableInstant time = (ReadableInstant) timeValue;
		//give data to agent
		this.getAgent().setData(newData, time);
	}

	@Override
	public void configureAgent(ProcessContext context) {
		//get ISA config
		Data data = null;
		String expandedUrlString = (String) ExpressionResolver.expand(
				urlStringISASetup, context, data);
		if (expandedUrlString == null) {
			log.error("can't find the file! {}", expandedUrlString);
		}
		URL url1;
		try {
			url1 = new URL(expandedUrlString);
			SourceURL url = new SourceURL(url1);
			JSONStream stream = new JSONStream(url);
			stream.init();
			data = stream.readNext();
		    } catch (Exception e) {
		      log.error("can't load from file! {}", expandedUrlString);
		      e.printStackTrace();
		}
		
		//get cells
		Serializable cellsValue = data.get("cells");
		Object obj=JSONValue.parse(cellsValue.toString());
		JSONArray array=(JSONArray)obj;
		Vector<Area> agentCells = new Vector<Area>();
		agentCells.ensureCapacity(array.size());
		for(int ind=0; ind< array.size(); ++ind){
			agentCells.add(Area.createFromJSON(array.get(ind).toString()));
		}
		//get models
		Serializable modelsValue = data.get("indepModels");
		obj=JSONValue.parse(modelsValue.toString());
		array=(JSONArray)obj;
		Vector<IModelIndep> agentModels = new Vector<IModelIndep>();
		agentModels.ensureCapacity(array.size());
		for(int ind=0; ind< array.size(); ++ind){
			agentModels.add(ModelIndepFactory.createFromJSON(array.get(ind).toString()));
		}
//		System.out.println(agentModels);
		
		//add
		this.getAgent().setAgentCells(agentCells);
		this.getAgent().setAgentModels(agentModels);
	}

	public String getUrlStringISASetup() {
		return urlStringISASetup;
	}

	public void setUrlStringISASetup(String urlStringISASetup) {
		this.urlStringISASetup = urlStringISASetup;
	}
	
}
