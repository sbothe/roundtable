package de.fhg.insight.roundTable.Bayes.stream.simulation.services;

import java.util.ArrayList;

import org.joda.time.Instant;

import de.fhg.insight.roundTable.Bayes.BayesEvent;
import stream.service.Service;

public interface SimulatorServices extends Service {

	public ArrayList<BayesEvent> getSimulatedEvents(); 
	public Instant getSimulationStartTime();
}
