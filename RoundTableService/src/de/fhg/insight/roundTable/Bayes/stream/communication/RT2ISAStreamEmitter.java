package de.fhg.insight.roundTable.Bayes.stream.communication;

import org.joda.time.ReadableInstant;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesAgent;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RT2ISAemitter;

public class RT2ISAStreamEmitter implements RT2ISAemitter<IBayesAgent> {

	@Override
	public void tellAgentToUpdateSearch(IBayesAgent participatingAgent,
			IBayesRoundTable<IBayesAgent> bayesRoundTable, Area searchArea2) {
		participatingAgent.updateArea(bayesRoundTable,searchArea2);
	}

	@Override
	public void tellAgentToJoin(IBayesAgent a,
			BayesRoundTable<IBayesAgent> table, Area area,
			ReadableInstant startTime) {
		a.joinTable(table, area, startTime);
	}


	@Override
	public void tellAgentToLeave(IBayesAgent a,
			IBayesRoundTable<IBayesAgent> bayesRoundTable) {
		a.leaveTable(bayesRoundTable);
	}
	
	@Override
	public RT2ISAStreamEmitter clone() throws CloneNotSupportedException {
        return (RT2ISAStreamEmitter) super.clone();
	}
}
