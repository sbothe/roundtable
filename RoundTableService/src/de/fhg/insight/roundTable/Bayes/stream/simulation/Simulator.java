package de.fhg.insight.roundTable.Bayes.stream.simulation;

import java.util.ArrayList;
import java.util.Collections;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;

import de.fhg.insight.roundTable.Bayes.BayesEvent;
import de.fhg.insight.roundTable.Bayes.EventGenerator;
import de.fhg.insight.roundTable.Bayes.TimeInterval;
import de.fhg.insight.roundTable.Bayes.World;
import de.fhg.insight.roundTable.Bayes.stream.simulation.services.SimulatorServices;
import stream.Data;
import stream.ProcessContext;
import stream.Processor;
import stream.StatefulProcessor;
import stream.io.TimeStream;
import stream.runtime.setup.handler.StreamElementHandler;
import stream.util.parser.TimeParser;

/*
 * @author francois
 * This class is responsible to generate all the elements of the simulation and to make them available to any other processor
 */
public class Simulator implements StatefulProcessor, SimulatorServices {
	static Logger log = LoggerFactory.getLogger(Simulator.class);

	//TODO: transform into a stream generating a simulation clock
	Duration simulationPeriod = null;// the virtual period between two heart beat of the simulation //new Duration(systemTimeIntervalMilli/1000);
	String keySimulationTime = "simulationTime";
	Instant simulationCurrentTime = null;
	DateTime startTime= new DateTime(2013, 01, 01, 0, 0); //y/m/d/h/m
	DateTime endTime= new DateTime(2013, 01, 01, 23, 59);
	TimeInterval systemTimeInterval= null;	//The interval of the whole simulation
	boolean hasNotSignaledSimulationOver = true;
	
	double dim = 1;
	World myWorld = null;

	EventGenerator myEventGenerator = null;
	int nbrEvents =1;
	ArrayList<BayesEvent> myEvents= null;
	
	//this method modifies a TimeStream to generate the heartbeat of the simulation
	// all because the privacy of the fields of TimeStram does not allow me to derive from it without using reflection
	@Override
	public Data process(Data input) {
		if (simulationCurrentTime.isAfter(endTime)){ //we are at the end of the simulation
			if(hasNotSignaledSimulationOver){
				hasNotSignaledSimulationOver = false;
				log.info("simulation over.");
			}
			return null;
		}
		else{
			input.put(keySimulationTime, simulationCurrentTime);
			simulationCurrentTime = simulationCurrentTime.plus(simulationPeriod); //move forward in time
			return input;
		}
	}

	@Override
	public ArrayList<BayesEvent> getSimulatedEvents() {
		if (myEvents == null){
			this.genEvents();
		}
		return myEvents;
	}

	private void genWorld(){
		Coordinate[] points = { 
			    new Coordinate(0.0,0.0),
			    new Coordinate(dim,0.0),
			    new Coordinate(dim,dim),
			    new Coordinate(0.0,dim),
			    new Coordinate(0.0,0.0),
			};
		GeometryFactory myGeometryFactory = new GeometryFactory();
		LinearRing myLinearRing= myGeometryFactory.createLinearRing(points);
		this.myWorld = new World(myLinearRing,myGeometryFactory.getPrecisionModel(),0);
	}
	
	private void genEvents(){
		systemTimeInterval = new TimeInterval(startTime,endTime);
		myEventGenerator = new EventGenerator(systemTimeInterval, myWorld);
		myEvents= myEventGenerator.generateEvents(nbrEvents);
		Collections.sort(myEvents, BayesEvent.BayesEventStartTimeComparator);
	}
	/*** life cycle ***/
	@Override	//this comes from Service
	public void reset() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(ProcessContext context) throws Exception {
		this.resetState();
	}

	@Override
	public void finish() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetState() throws Exception { //this comes from StatefulProcessor
		myEvents = null;
		this.genWorld();
		simulationCurrentTime = new Instant(startTime);
		hasNotSignaledSimulationOver = true;
	}
	
	/*** getters and setters ***/
	public DateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		
		this.startTime = this.string2DateTime(startTime);
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = this.string2DateTime(endTime);
	}

	public int getNbrEvents() {
		return nbrEvents;
	}

	public void setNbrEvents(int nbrEvents) {
		this.nbrEvents = nbrEvents;
	}

	public double getDim() {
		return dim;
	}

	public void setDim(double dim) {
		this.dim = dim;
	}

	@Override
	public Instant getSimulationStartTime() {
		return new Instant(startTime);
	}
	
	public Duration getSimulationPeriod() {
		return simulationPeriod;
	}

	public void setSimulationPeriod(String simulationPeriod) {
		Long periodMilli;
		try {
			periodMilli = TimeParser.parseTime(simulationPeriod);
		} catch (Exception e) {
			periodMilli = -1L;
			throw new RuntimeException("Invalid time string '" + simulationPeriod
					+ "': " + e.getMessage());
		}
		this.simulationPeriod = new Duration(periodMilli);
	}

	public String getKeySimulationTime() {
		return keySimulationTime;
	}

	public void setKeySimulationTime(String keySimulationTime) {
		this.keySimulationTime = keySimulationTime;
	}

	private DateTime string2DateTime(String input){	
		DateTime output = new DateTime(input);
		return output;
	}
}
