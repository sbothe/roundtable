package de.fhg.insight.roundTable.Bayes.stream.simulation;

import de.fhg.insight.roundTable.Bayes.simulation.BayesAgentCellSimulated;
import de.fhg.insight.roundTable.Bayes.simulation.BayesAgentCellSimulatedEventType;

public class BayesISACellProcessSimulatedEventType extends
		AbstractBayesISACellProcessSimulated<BayesAgentCellSimulatedEventType<Long>> {

	@Override
	protected BayesAgentCellSimulated<Long> constructAgent() {
		return new BayesAgentCellSimulatedEventType<Long>();
	}
}
