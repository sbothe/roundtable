package de.fhg.insight.roundTable.Bayes.stream.communication;

import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesAgent;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Interfaces.IRoundTableManager;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.IISA2RTemitter;

public class ISA2RTStreamEmitter implements IISA2RTemitter<IBayesRoundTable> {
	IRoundTableManager roundTableManager = null;
	
	public ISA2RTStreamEmitter(IRoundTableManager roundTableManager) {
		super();
		this.roundTableManager = roundTableManager;
	}

	public void addData(BayesRoundTableData data, IBayesRoundTable table, IBayesAgent<IBayesRoundTable> agent) {
		//TODO: maybe put on cardsOn?
		table.receiveData(data,agent);	
	}
	
	public void newAnomaly(BayesRoundTableData anomalousData, IBayesAgent<IBayesRoundTable> agent) {
		this.roundTableManager.newAnomaly(anomalousData, agent);
	}

	public IRoundTableManager getRoundTableManager() {
		return roundTableManager;
	}

	public void setRoundTableManager(IRoundTableManager roundTableManager) {
		this.roundTableManager = roundTableManager;
	}
	
	
}
