package de.fhg.insight.roundTable.Bayes.stream;

import java.io.Serializable;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesAgent;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.IISA2RTemitter;
import de.fhg.insight.roundTable.Bayes.stream.messageType.rtToISA;
import stream.Data;
import stream.ProcessContext;
import stream.StatefulProcessor;
import stream.data.DataFactory;
import stream.io.Sink;
/*
 * @author fschnitzler
 * This abstract class is a stream process encapsulating an ISA.
 * This level of the class hierarchy is responsible to route the communication 
 * 	with other RT components within the streams framework.
 * TODO: extract information here?
 */
public abstract class AbstractBayesISAprocess<TagentType extends IBayesAgent<Long> > implements StatefulProcessor, IISA2RTemitter<Long>{
	//TODO: allow modification of streamtype keys
	Sink output1 = null; //output1: roundTableManagerSink. More explicit name not possible because of streams library bug
    Sink output2 = null; //output2: roundTableSink
	static Logger log = LoggerFactory.getLogger(AbstractBayesISAprocess.class);

	private TagentType agent = null;
	//protected Class<TagentType> classAgent;
	
	String keyStreamType = "@StreamType";		//key to identify a stream
	String keyMessageType = "@MessageType";		//key to identify a message sent on a stream
	String keyRoundTableID = "roundTableID";
	String keyArea = "area";
	String keyStartTime = "startTime";
	String keyRoundTableData = "roundTableData";
	String keyIntelligentSensorAgentID = "intelligentSensorAgentID";
	
	String id = null;
	/*** incoming communication ***/
	
	@Override
	public Data process(Data item) {
		// get stream type
		Serializable valueStreamType = item.get( keyStreamType ); // item.get( key )
		String streamType = valueStreamType.toString();
		// switch based on type
		log.debug("receiving message of type {}",streamType.toLowerCase());
		switch (streamType.toLowerCase()) {
		case "simul":
			this.processSimul(item);
			break;
		case "data":
			this.processData(item);
			break;
		case "query":
			this.processQuery(item);
			break;
		default: 
			log.info("Error: unknown event type {}", streamType);
        break;	
		}
		return item;
	}

	protected void processQuery(Data item) {
		// get stream type
		Serializable valueMessageType = item.get( keyMessageType ); // item.get( key )
		rtToISA messageType = (rtToISA) valueMessageType;
		switch(messageType){
		case JOIN:
			Serializable valueRoundTableID = item.get( keyRoundTableID ); 
			Long roundTableID = this.readRoundTableID(valueRoundTableID);
			log.debug("ISA {} told to join table {}",this.getId(),roundTableID);
			Serializable valueArea = item.get( keyArea ); 
			Area area = (Area) valueArea;
			Serializable valueInstant = item.get( keyStartTime ); 
			DateTime startTime = (DateTime) valueInstant;
			this.agent.joinTable(roundTableID, area, startTime);
			break;
		case LEAVE:
			valueRoundTableID = item.get( keyRoundTableID ); 
			roundTableID = this.readRoundTableID(valueRoundTableID);
			log.debug("ISA {} told to leave table {}",this.getId(),roundTableID);
			this.agent.leaveTable(roundTableID);
			break;
		case INFO:	// obtain the info from the agent (its ID) and send it to the requester.
			//TODO: do this as a service
			//generate message
			Data message = DataFactory.create();
			//TODO: better info request
			message.put("info", this.agent.getID());
			//get stream where it should be sent
			Serializable valueTargetStream = item.get("infoTargetStream" ); // item.get( key )
			String targetStream = valueTargetStream.toString();
			try {
				log.debug("Processing info request...");
				switch(targetStream){
				case "tableManager":
					output1.write(message);
					break;
				case "tables":
					output2.write(message);
					break;
				default:
					log.error("Improper info request stream '{}",targetStream);
				}
			} catch (Exception e) {
				log.error("Failed to send agent info requested output sink '{}'",targetStream);
				if (log.isDebugEnabled())
					e.printStackTrace();
			}
						
			break;
		case UPDATE:
			valueRoundTableID = item.get( keyRoundTableID ); 
			roundTableID = this.readRoundTableID(valueRoundTableID);
			log.debug("ISA {} received update from table {}",this.getId(),roundTableID);
			valueArea = item.get( keyArea ); 
			area = (Area) valueArea;
			this.agent.updateArea(roundTableID, area);
			break;
		default:
			log.info("Error: unknown RT message type {}", messageType);
		}
	}
	//process specific information related to simulation
	protected abstract void processSimul(Data item);

	//process incoming data
	protected abstract void processData(Data item);
	
	/*** outgoing communication: from the ISA to the RT ***/
	/*
	 * This method sends a particular @data to a given round @table. @sensor is the ISA which generated the data.
	 */
	@Override
	public void addData(BayesRoundTableData data, Long table, IBayesAgent<Long> sensor){
		//create stream message
		//message content
		Data message = DataFactory.create();
		message.put(keyRoundTableData, data);
		message.put(keyRoundTableID, table);
		message.put(keyIntelligentSensorAgentID, sensor.getID()); // the RT receives receive streams id
		//message meta information
		message.put(keyStreamType, "answer");

		//send the message
		try {
			log.debug("Trying to send answer {}",message);
			this.output2.write(message);
		} catch (Exception e) {
			log.error("Failed to write answer item to output sink '{}': {}",
					this.output2, e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
	}

	/*
	 * This method reports on a new @anomalousData. @sensor is the ISA which generated the data.
	 */
	@Override
	public void newAnomaly(BayesRoundTableData anomalousData, IBayesAgent<Long> sensor){
		//create stream message
		Data message = DataFactory.create();
		message.put(keyRoundTableData, anomalousData);
		message.put(keyIntelligentSensorAgentID, sensor.getID());
		//message meta information
		message.put(keyStreamType, "anomaly");

		//send the message
		try {
			log.debug("Sending anomaly...");
			this.output1.write(message);
		} catch (Exception e) {
			log.error("Failed to write anomaly item to output sink '{}': {}",
					this.output1, e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
	}
	
	/*** process life cycle ***/
	@Override
	public void finish() throws Exception {
		// TODO remove registration from the ISA. Leave all round tables
		
	}
	
	@Override
	public void init(ProcessContext arg0) throws Exception {
		//TODO: set agent ID
		//this.agent.setID();
		this.setAgent(this.constructAgent());
		this.agent.setId(this.id);
		this.configureAgent(arg0);	
		//give emitter
		this.agent.setISA2RTemitter(this);
	}
	
	//This function must creates an agent of the correct type
	protected abstract TagentType constructAgent();

	@Override
	public void resetState() throws Exception {
		//TODO implement
		//this.agent.reset();
	}

	// add all the necessary configuration of an agent
	public abstract void configureAgent(ProcessContext arg0);
	
	/*** utility ***/
	//recover the roundTableID from a stream message
	public Long readRoundTableID(Serializable value){
		Long tmp = (Long) value;
		return tmp;
	}

	/*** getters and setters ***/ 
	//output1: roundTableManagerSink
	//output2: roundTableSink
	public Sink getOutput1() {
		return output1;
	}

	public void setOutput1(Sink roundTableManagerSink) {
		this.output1 = roundTableManagerSink;
	}
	//output2: roundTableSink
	public Sink getOutput2() {
		return output2;
	}

	public void setOutput2(Sink output2) {
		this.output2 = output2;
	}

	public String getKeyStreamType() {
		return keyStreamType;
	}

	public void setKeyStreamType(String keyStreamType) {
		this.keyStreamType = keyStreamType;
	}

	public String getKeyMessageType() {
		return keyMessageType;
	}

	public void setKeyMessageType(String keyMessageType) {
		this.keyMessageType = keyMessageType;
	}

	public String getKeyRoundTableID() {
		return keyRoundTableID;
	}

	public void setKeyRoundTableID(String keyRoundTableID) {
		this.keyRoundTableID = keyRoundTableID;
	}

	public String getKeyArea() {
		return keyArea;
	}

	public void setKeyArea(String keyArea) {
		this.keyArea = keyArea;
	}

	public String getKeyStartTime() {
		return keyStartTime;
	}

	public void setKeyStartTime(String keyStartTime) {
		this.keyStartTime = keyStartTime;
	}

	public String getKeyRoundTableData() {
		return keyRoundTableData;
	}

	public void setKeyRoundTableData(String keyRoundTableData) {
		this.keyRoundTableData = keyRoundTableData;
	}

	public String getKeyIntelligentSensorAgentID() {
		return keyIntelligentSensorAgentID;
	}

	public void setKeyIntelligentSensorAgentID(String keyIntelligentSensorAgentID) {
		this.keyIntelligentSensorAgentID = keyIntelligentSensorAgentID;
	}

	public TagentType getAgent() {
		return agent;
	}

	public void setAgent(TagentType agent) {
		this.agent = agent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		if (this.agent!=null)
			this.agent.setId(this.id);
	}

	
}
