package de.fhg.insight.roundTable.Bayes.communication;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableStatus;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RToutputEmitter;

public class RToutputDefaultEmitter implements RToutputEmitter{

	final Logger log = LoggerFactory.getLogger(RToutputDefaultEmitter.class);
	
	@Override
	public void emitReport(Long roundTableID, Instant currentTime,
			BayesRoundTableData event, BayesRoundTableStatus roundTableStatus) {
		log.info("time {}, RT {} ({}) output {}",currentTime, roundTableID, roundTableStatus,event);
	}
}
