package de.fhg.insight.roundTable.Bayes;

import java.io.IOException;

import org.geotools.geojson.geom.GeometryJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;

import de.fhg.insight.roundTable.Tests.TestStreamingBayesRoundTable;

// description of the world
// TODO: use ontology
public class World extends Area{
	final Logger log = LoggerFactory.getLogger(World.class);
	public World(LinearRing shell, LinearRing[] holes,
			PrecisionModel precisionModel, int SRID) {
		super(shell, holes, precisionModel, SRID);
		// TODO Auto-generated constructor stub
	}

	public World(LinearRing shell, PrecisionModel precisionModel, int SRID) {
		super(shell, precisionModel, SRID);
		// TODO Auto-generated constructor stub
	}

	public World(LinearRing shell, LinearRing[] holes, GeometryFactory factory) {
		super(shell, holes, factory);
		// TODO Auto-generated constructor stub
	}
	public World(Polygon other){
		super(other);
	}
	static World createFromJSON(String worldJSON){
		GeometryJSON g = new GeometryJSON();
		Polygon recovered = null;
		try {
			recovered = g.readPolygon(worldJSON);
		} catch (IOException e) {
			//log.eror("could not create world from {}", worldJSON);
			e.printStackTrace();
		}
		return new World(recovered); 
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 6683243037159051918L;

	public boolean contains(Area other){
		return this.contains(other);
	}
}
