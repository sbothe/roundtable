package de.fhg.insight.roundTable.Bayes;

import java.io.Serializable;

public class BayesRoundTableData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2411029618177564223L;

	int id;
	Area area;
	TimeInterval interval;
	OntologyDistribution eventTypeDistribution = null;
	//BayesEvent event;
	//Double probability;	// empty = question
	Double relevance;   // empty = question
	public BayesRoundTableData(int id, Area area, TimeInterval interval,
			OntologyDistribution eventTypeDistribution, Double relevance) {
		super();
		this.id = id;
		this.area = area;
		this.interval = interval;
		this.eventTypeDistribution = eventTypeDistribution;
		this.relevance = relevance;
	}
	public BayesRoundTableData(BayesRoundTableData rtData) {
		this.id = rtData.id;
		this.area = new Area(rtData.getArea());
		this.interval = rtData.getInterval();
		this.eventTypeDistribution = rtData.eventTypeDistribution;
		this.relevance = rtData.relevance;

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public TimeInterval getInterval() {
		return interval;
	}
	public void setInterval(TimeInterval interval) {
		this.interval = interval;
	}
	public OntologyDistribution getEventTypeDistribution() {
		return eventTypeDistribution;
	}
	public void setEventTypeDistribution(OntologyDistribution eventTypeDistribution) {
		this.eventTypeDistribution = eventTypeDistribution;
	}
	public Double getRelevance() {
		return relevance;
	}
	public void setRelevance(Double relevance) {
		this.relevance = relevance;
	}
	public void set(BayesEventType eventType, Double proba){
		this.eventTypeDistribution.put(eventType, proba);
	}
	
	public  String toString(){
		return "RTD id " + this.getId() + ", area " + this.getArea() + ", time " + this.getInterval() + ", type " + this.getEventTypeDistribution(); 
	}

	
}
