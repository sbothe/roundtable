package de.fhg.insight.roundTable.Bayes.hardCodedEventType;

public enum simulationBayesEventType {
	
		ROOT, 		//lvl 0
		NORMAL, 	//lvl 1
		ANOMALY, 
		FLOOD,	 	//lvl 2
		FIRE,
		SNOW,
		TRAFFIC_JAM;
}
