package de.fhg.insight.roundTable.Bayes.hardCodedEventType;

public enum InsightDCCDemoBasic {
	ROOT, 		//lvl 0
	NORMAL, 	//lvl 1
	ANOMALY;
}
