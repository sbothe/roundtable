package de.fhg.insight.roundTable.Bayes;

public enum BayesRoundTableStatus {
	TOO_SHORT,
	IN_PROGRESS,
	END,
	KILLED
}
