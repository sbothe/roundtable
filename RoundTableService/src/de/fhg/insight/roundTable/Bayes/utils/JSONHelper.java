package de.fhg.insight.roundTable.Bayes.utils;

import java.util.ArrayList;
import java.util.Vector;

import de.fhg.insight.roundTable.Bayes.Model.IModelIndep;
import de.fhg.insight.roundTable.Bayes.Model.ModelIndepFactory;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONValue;

//TODO: this is not working, damn JAVA limitations.
public class JSONHelper {
	public static <T> Vector<T> parseJsonArray(JSONArray array){
		return JSONHelper.<T,T>parseJsonArrayFactory(array);
	}
	public static <T,Tfactory> Vector<T> parseJsonArrayFactory(JSONArray array){
		//Object obj=JSONValue.parse(cellsValue.toString());
		//JSONArray array=(JSONArray)obj;
		Vector<T> outputArrayList = new Vector<T>();
		outputArrayList.ensureCapacity(array.size());
		for(int ind=0; ind< array.size(); ++ind){
			outputArrayList.add(Tfactory.createFromJSON(array.get(ind).toString()));
		}
		return null;
	}
	public static <T> Vector<T> parseJsonArrayFromString(String input){
		return JSONHelper.<T,T>parseJsonArrayFromStringFactory(input);
	}
	public static <T,Tfactory> Vector<T> parseJsonArrayFromStringFactory(String input){
		Object obj=JSONValue.parse(input);
		JSONArray array=(JSONArray)obj;
		return JSONHelper.<T,Tfactory>parseJsonArrayFactory(array);
	}
}
