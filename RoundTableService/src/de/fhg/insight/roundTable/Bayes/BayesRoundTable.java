package de.fhg.insight.roundTable.Bayes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
//import java.util.Map;
//import java.util.Calendar;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.joda.time.ReadableInstant;
import org.slf4j.Logger;	
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Interfaces.IRoundTableManager;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RT2ISAemitter;
import de.fhg.insight.roundTable.Bayes.LabelInference.ILabelInferenceAlgorithm;

/**
 * 
 * @author fschnitzler
 * @author tliebig
 * 
 * This class aggregates probabilistic data (conditionnal densities) from an agent.
 */
public class BayesRoundTable<TBayesAgent>  implements IBayesRoundTable<TBayesAgent>{
	RT2ISAemitter<TBayesAgent> emitter = null;

	final Logger log = LoggerFactory.getLogger(BayesRoundTable.class);
	static int nextId =0;
	Long id ;
	private IRoundTableManager roundTableManager = null;
	private LinkedList<TBayesAgent> participants = new LinkedList<TBayesAgent>();
	private HashMap<TBayesAgent, ArrayList<BayesRoundTableData> > OnTableData = new HashMap<TBayesAgent,  ArrayList<BayesRoundTableData> >();
	private BayesRoundTableData result = null;
	private BayesRoundTableData issue = null;
	private TBayesAgent startingAgent = null;
	private Area searchArea = null;
	private Instant latestTimeReceived = null;
	boolean hasBeenClosed = false;
	private BayesRoundTableStatus currentStatus = BayesRoundTableStatus.IN_PROGRESS;
	
	ILabelInferenceAlgorithm labelInferenceAlgorithm = null;
	// TODO: assumption: all possible labels are provided by the agent calling for the round table

	// private TreeSet<Calendar> data_times = new TreeSet<Calendar>;
	
	int minimumDurationOfEvent = 300000; //default: 5 minutes (in milliseconds)
	int timeWithoutSupportingDataBeforeClosing = 300000; //default: 5 minutes (in milliseconds)

	// -----Konstruktor-----
	public BayesRoundTable(BayesRoundTableData issue, TBayesAgent startingAgent,IRoundTableManager roundTableManager) {
		try {
			this.setIssue(issue);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.startingAgent = startingAgent;
		this.roundTableManager=roundTableManager;
		this.setInitialSearchArea();
		this.id=new Long(nextId);
		++nextId;
		log.debug("starting RT {} for issue {}", this.id,  this.issue);
	}

	// -------Methods-------

	//TODO: do something a little bit more clever.
	private void setInitialSearchArea() {
		Area sourceArea = this.getIssue().getArea();
		this.searchArea = sourceArea.scale(2);
	}

	// add an Agent to the Table
	public boolean addAgent(TBayesAgent a) {
		log.debug("telling agent {} to join",a);
		this.tellAgentToJoin(a,this, this.getSearchArea(a), this.getStartTime(a));
		this.OnTableData.put(a, new ArrayList<BayesRoundTableData>());
		return ((this.participants.add(a)));
	}

	private DateTime getStartTime(TBayesAgent a) {
		return this.getIssue().getInterval().getStart();
	}

	//TODO: adapt to sensors.
	private Area getSearchArea(TBayesAgent a) {
		return this.searchArea;
	}

	@Override
	public void receiveData(BayesRoundTableData data, TBayesAgent a) {
		log.debug("receiving RTD {}",data.getId());
		if(this.isAlive()){	//only do something if the RT is still alive.
			//only discuss if new data arrived later than the previous data
			//TODO: allow additional mechanism to trigger discussion. Maybe manager? or clock?
			Instant endTimeData = new Instant(data.getInterval().getEnd());
			if (this.latestTimeReceived != null) 
				if (this.latestTimeReceived.isBefore(endTimeData))
					this.discuss();	//might kill the RT
			
			//add the new data
			this.latestTimeReceived = endTimeData;
			if(this.isAlive())
				OnTableData.get(a).add(data);
		}
	}

	private boolean isAlive() {
		// table is dead when OnTableData is null
		return (OnTableData!=null);
	}

	//TODO should this be moved to the RT manager?
	private void checkStatus(BayesRoundTableData previousResult,
			BayesRoundTableData newResult) {
		// check if RT must be killed because no update for too long
		//TODO: output better
		if ((this.latestTimeReceived.getMillis() -newResult.getInterval().getEndMillis()) > timeWithoutSupportingDataBeforeClosing){ //no data supporting anomaly since 5 minutes
			//check event length 
			if((newResult.getInterval().getEndMillis() - newResult.getInterval().getStartMillis()) < minimumDurationOfEvent){//if event lasted less than 5 minutes, dismiss
				log.debug("killing RT {}, too short",this.id);
				this.setStatus( BayesRoundTableStatus.TOO_SHORT );
				this.selfKill();
			}
			else{
				log.debug("killing RT {}, event over. Conclusion: {}",this.id, newResult);
				this.setStatus( BayesRoundTableStatus.END );
				this.selfKill();
			}
		}
		//check if search area must be increased
		else if(this.searchArea.getBoundary().intersects(newResult.getArea().getBoundary())){
			this.searchArea = this.searchArea.scale(1.2); //increase by 20 %
			log.debug("increasing search area for RT {} to {}",this.id ,this.searchArea);
			this.updateRequestsToagents();
		}
	}
	
	//this function emits a report on the situation, transmitted by the RT manager.
	private void emitReport(BayesRoundTableStatus status){
		log.debug("RT {} ({}) tries to emit report.", this.getId(), status);
		this.roundTableManager.emitReport(this.getId(), this.getCurrentTime(), this.getResult(), status);
	}

	private Instant getCurrentTime() {
		DateTime temp = new DateTime();
		return temp.toInstant();
	}

	private void selfKill() {
		this.close();
		this.roundTableManager.killRoundTable(this);
	}

	//prepare destruction
	public void close() {
		if (!hasBeenClosed){
			log.debug("RT {} preparing for destruction",this.id);
			this.emitReport(currentStatus);
			
			while(!this.participants.isEmpty()){
				TBayesAgent participatingAgent = this.participants.getFirst();
				this.deleteAgent(participatingAgent);
			}
			OnTableData = null;
			hasBeenClosed = true;
		}
	}
	
	@Override
	public void setStatus(BayesRoundTableStatus newStatus){
		this.currentStatus = newStatus;
	}

	// delete an Agent (if he is useless or whatever)
	public boolean deleteAgent(TBayesAgent a) {
		if (!(this.participants.remove(a))) {
			System.out.println("Something failed removing the Agent for"
					+ a);
			return false;
		}
		this.OnTableData.remove(a);
		this.tellAgentToLeave(a,this);
		return true;
	}

	// set the Issue to discuss based on given data
	// store location and time
	public void setIssue(BayesRoundTableData rtData)
			throws CloneNotSupportedException {
		if (this.issue == null) {
			this.issue = rtData;
			this.result = new BayesRoundTableData(rtData);
		} else {
			System.out
					.println("You are not allowed to set a new Issue (because we are just discussing one)");
		}
		this.result=rtData;
	}

	public BayesRoundTableData getIssue() {
		return this.issue;
	}

	public BayesRoundTableData discuss(){
		log.debug("round table starting dicussing {} based on data untill {} ",this.getIssue(), this.latestTimeReceived );
		BayesRoundTableData previousResult = this.result; 
		BayesRoundTableData newResult = this.discussData();
		this.checkStatus( previousResult, newResult);
		return newResult;
	}
	// discuss the information provided by the agents
	// At the moment: the round table uses the best semantic information
	// TODO: refactorize

	public BayesRoundTableData discussData() {
		//initialize (warm start?)
		Area currentArea = this.issue.getArea();
		TimeInterval currentInterval = this.issue.getInterval();
		
		//TODO integrate events
		//EventType currentType =
		
		//iterate until stabilisation (EM like?)
		//TODO: better congergence system
		int itInd = 0;
		while(itInd < 10){
			//update time first: you want to integrate these new data.
			currentInterval = updateTime(currentArea, currentInterval);
			//update location
			//force the area to contain original data.
			//penalizes larger circle
			currentArea = updateArea(currentArea, currentInterval);
			//TODO
			//update eventType more regularly?

			//check convergence 
			++itInd;
		}
		//TODO: add type
		//label = updateType(currentArea,currentInterval);
		OntologyDistribution currentLabelLikelihoods = this.updateOntologyDistribution(currentArea, currentInterval);
		
		this.result = new BayesRoundTableData(0, currentArea, currentInterval, currentLabelLikelihoods, 0.0);
		return this.result;
		//tells the ISA to get ready if necessary?
		//for (TBayesAgent a : participants) {
		//	a.collectInformation();
		//}
		//gets the data
		// The agent is streaming the data
		//for (TBayesAgent a : participants) {
		//	a.cardsOn(); // should crush data
		//}
		//aggregate information
//		ProbabilisticEventData resultProba = naiveBayes();
//		double acc;
//		for (RoundTableDataExample d : OnTableData.values()) {
//			if (!(d == null)) {
//				acc = result.getAccuracy();
//				if (d.getRange_event() < result.getRange_event()) {
//					result.setEvent(d.getEvent());
//				}
//			}
//		}
//		result.setEventData(resultProba);
		
//		return null;
	}

	private void sendMatchingDataToInferenceAlgorithm(Area currentArea,
			TimeInterval currentInterval){
		labelInferenceAlgorithm.receiveData(this.getIssue());
		//operate on all data
		for(Map.Entry<TBayesAgent, ArrayList<BayesRoundTableData>> entry : this.OnTableData.entrySet()){
			for(BayesRoundTableData dataItem : entry.getValue()){
				//check whether data match
				boolean areaMatch = dataItem.getArea().intersects(currentArea);
				boolean timeMatch = dataItem.getInterval().overlaps(currentInterval);
				if( areaMatch &&  timeMatch){
					//if match, add data
					labelInferenceAlgorithm.receiveData(dataItem);
				}
			}
		}
	}
	private BayesEventType updateType(Area currentArea,
			TimeInterval currentInterval) {
		BayesEventType result = null;
		if (labelInferenceAlgorithm == null){
			Entry<BayesEventType, Double> maxEntry = null;

			for (Entry<BayesEventType, Double> entry : this.getIssue().getEventTypeDistribution().entrySet())
			{
				if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
				{
					maxEntry = entry;
				}
			}
			result = maxEntry.getKey();
		}
		else{
			labelInferenceAlgorithm.reset();
			//operate on all data
			this.sendMatchingDataToInferenceAlgorithm(currentArea, currentInterval);
			//get result
			result=this.labelInferenceAlgorithm.getBestGuess();
		}
		
		return result;
	}
	
	private OntologyDistribution updateOntologyDistribution(Area currentArea,
			TimeInterval currentInterval) {
		OntologyDistribution result = new OntologyDistribution(BayesEventType.class);
		if (labelInferenceAlgorithm == null){
			result.put(this.updateType(currentArea, currentInterval), 1.0);
		}
		else{
			labelInferenceAlgorithm.reset();
			//operate on all data
			this.sendMatchingDataToInferenceAlgorithm(currentArea, currentInterval);
			//get result
			result=this.labelInferenceAlgorithm.infer();
		}
		
		return result;
	}

	private Area updateArea(Area origArea, TimeInterval currentInterval) {
		//get original area data (must always contain)
		// 0.99: avoid neighboring cells
		//TODO: do not do that if this is a point.
		Area shrinkedArea = this.issue.getArea().scale(0.99);
		double maxXLeft=shrinkedArea.getEnvelopeInternal().getMinX();
		double minXRight=shrinkedArea.getEnvelopeInternal().getMaxX();
		double maxYBot=shrinkedArea.getEnvelopeInternal().getMinY();
		double minYTop=shrinkedArea.getEnvelopeInternal().getMaxY();
		
		//TODO" do better than this, we can have data outside the searchArea
		//TODO: limit to the World
		double minXLeft=this.searchArea.getEnvelopeInternal().getMinX();
		double maxXRight=this.searchArea.getEnvelopeInternal().getMaxX();
		double minYBot=this.searchArea.getEnvelopeInternal().getMinY();
		double maxYTop=this.searchArea.getEnvelopeInternal().getMaxY();
		
		double currentXLeft=origArea.getEnvelopeInternal().getMinX();
		double currentXRight=origArea.getEnvelopeInternal().getMaxX();
		double currentYBot=origArea.getEnvelopeInternal().getMinY();
		double currentYTop=origArea.getEnvelopeInternal().getMaxY();
		
		//TODO: better search than this step
		int div=5;
		double stepXLeft= (maxXLeft-minXLeft)/div;
		double stepXRight= (maxXRight-minXRight)/div;
		double stepYBot= (maxYBot-minYBot)/div;
		double stepYTop= (maxYTop-minYTop)/div;
		//iterate until convergence
		for(int itInd=0; itInd<10; ++itInd){
			//** left side
			//alternate over steps
			int bestStepInd=0;
			double bestScore=Double.NEGATIVE_INFINITY;
			for(int stepInd=0; stepInd<div+1; ++stepInd){
				//area to test
				Area areaToTest = origArea.makeRectangle(minXLeft+stepInd*stepXLeft, currentXRight, currentYBot, currentYTop);
				double score = this.getScore(areaToTest, currentInterval);
				if (score>bestScore){ //keep the largest
					bestStepInd=stepInd;
					bestScore=score;
				}
			}
			currentXLeft=minXLeft+bestStepInd*stepXLeft;
			//** right side
			//alternate over steps
			bestStepInd=0;
			bestScore=Double.NEGATIVE_INFINITY;
			for(int stepInd=0; stepInd<div+1; ++stepInd){
				//area to test
				Area areaToTest = origArea.makeRectangle(currentXLeft, minXRight+stepInd*stepXRight, currentYBot, currentYTop);
				double score = this.getScore(areaToTest, currentInterval);
				if (score>=bestScore){ //keep the largest
					bestStepInd=stepInd;
					bestScore=score;
				}
			}
			currentXRight=minXRight+bestStepInd*stepXRight;
			//** bot side
			//alternate over steps
			bestStepInd=0;
			bestScore=Double.NEGATIVE_INFINITY;
			for(int stepInd=0; stepInd<div+1; ++stepInd){
				//area to test
				Area areaToTest = origArea.makeRectangle(currentXLeft, currentXRight, minYBot+stepInd*stepYBot, currentYTop);
				double score = this.getScore(areaToTest, currentInterval);
				if (score>bestScore){ //keep the largest
					bestStepInd=stepInd;
					bestScore=score;
				}
			}
			currentYBot=minYBot+bestStepInd*stepYBot;
			//** top side
			//alternate over steps
			bestStepInd=0;
			bestScore=Double.NEGATIVE_INFINITY;
			for(int stepInd=0; stepInd<div+1; ++stepInd){
				//area to test
				Area areaToTest = origArea.makeRectangle(currentXLeft, currentXRight, currentYBot, minYTop+stepInd*stepYTop);
				double score = this.getScore(areaToTest, currentInterval);
				if (score>=bestScore){ //keep the largest
					bestStepInd=stepInd;
					bestScore=score;
				}
			}
			currentYTop=minYTop+bestStepInd*stepYTop;
		}
		
		//iterate until 
		return origArea.makeRectangle(currentXLeft, currentXRight, currentYBot, currentYTop);
	}

	//get a score for anomalies on that area
	//computes a likelihood?
	private double getScore(Area areaToTest, TimeInterval currentInterval) {
		double logLikelihood=0.0;
		//iterate over all data
		for(Map.Entry<TBayesAgent, ArrayList<BayesRoundTableData>> entry : this.OnTableData.entrySet()){
			for(BayesRoundTableData dataItem : entry.getValue()){
				//does it match?
				// add likelihood of anomaly if it matches, or normal if it does not.
				boolean areaMatch = dataItem.getArea().intersects(areaToTest);
				boolean timeMatch = dataItem.getInterval().overlaps(currentInterval);
				if( areaMatch &&  timeMatch){
					logLikelihood += Math.log(dataItem.getEventTypeDistribution().getLikelihoodEventType(BayesEventType.ANOMALY));
				}
				else{
					logLikelihood += Math.log(dataItem.getEventTypeDistribution().getLikelihoodEventType(BayesEventType.NORMAL));
				}
			}
		}
		return logLikelihood;
	}

	private TimeInterval updateTime(Area currentArea,
			TimeInterval currentInterval) {
		DateTime startTime = currentInterval.getStart();
		DateTime currEndTime = currentInterval.getEnd();
		double latesTimeReceivedMilli = this.latestTimeReceived.getMillis()+1; //+1 because interval is open at the end
		double currEndTimeMilli = currEndTime.getMillis();
		int div = 5;
		double step = (latesTimeReceivedMilli - currEndTimeMilli)/div;
		double bestStepInd=0;
		double bestScore=Double.NEGATIVE_INFINITY;
		for(int stepInd=0; stepInd<div+1; ++stepInd){
			double score = this.getScore(currentArea, new TimeInterval(startTime, currEndTime.plus((long) (step*stepInd))));
			if (score>bestScore){ //keep the lowest, be conservative
				bestStepInd=stepInd; 
				bestScore=score;
			}
		}
		return new TimeInterval(startTime, currEndTime.plus((long) (step*bestStepInd)));
	}

	public void DataOnTable() {
		System.out.println("What lies on table: ");
		for (TBayesAgent a : OnTableData.keySet()) {
			try {
				System.out.println(OnTableData.get(a).toString());
			} catch (NullPointerException e) {
				System.out.println("Nothing");
			}
		}

	}

	public void printResult() {
		System.out.println("The result of the discussion is "
				+ this.getResult().toString());
	}
	
	

	public BayesRoundTableData getResult() {
		return result;
	}

	@Override
	public boolean removeAgent(TBayesAgent a) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BayesEvent getExplanation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateRequestsToagents() {
		for(TBayesAgent participatingAgent : this.participants){
			tellAgentToUpdateSearch(participatingAgent,this,this.searchArea);
		}
	}
	
	//------- emission: communication with the ISA -------
	private void tellAgentToUpdateSearch(TBayesAgent participatingAgent,
			BayesRoundTable<TBayesAgent> table, Area searchArea2) {
		this.getEmitter().tellAgentToUpdateSearch(participatingAgent,table,searchArea2);
	}

	public void tellAgentToJoin(TBayesAgent a, BayesRoundTable<TBayesAgent> table, Area area, DateTime startTime){
		this.getEmitter().tellAgentToJoin(a,table,area,startTime);
	}

	private void tellAgentToLeave(TBayesAgent a,
			BayesRoundTable<TBayesAgent> bayesRoundTable) {
		this.getEmitter().tellAgentToLeave(a,bayesRoundTable);
	}
	
	public RT2ISAemitter<TBayesAgent> getEmitter() {
		return emitter;
	}

	public void setEmitter(RT2ISAemitter<TBayesAgent> emitter) {
		this.emitter = emitter;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = new Long(id);
	}

	public int getMinimumDurationOfEvent() {
		return minimumDurationOfEvent;
	}

	public void setMinimumDurationOfEvent(int minimumDurationOfEvent) {
		this.minimumDurationOfEvent = minimumDurationOfEvent;
	}

	public int getTimeWithoutSupportingDataBeforeClosing() {
		return timeWithoutSupportingDataBeforeClosing;
	}

	public void setTimeWithoutSupportingDataBeforeClosing(
			int timeWithoutSupportingDataBeforeClosing) {
		this.timeWithoutSupportingDataBeforeClosing = timeWithoutSupportingDataBeforeClosing;
	}

	public ILabelInferenceAlgorithm getLabelInferenceAlgorithm() {
		return labelInferenceAlgorithm;
	}

	public void setLabelInferenceAlgorithm(
			ILabelInferenceAlgorithm labelInferenceAlgorithm) {
		this.labelInferenceAlgorithm = labelInferenceAlgorithm;
	}
	
	
}