package de.fhg.insight.roundTable.Bayes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import myRandom.MyRandom;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.base.BaseDuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;

public class EventGenerator {
	private static final List<BayesEventType> TYPEVALUES =
			    Collections.unmodifiableList(Arrays.asList(BayesEventType.values()));
	final static Logger log = LoggerFactory.getLogger(EventGenerator.class);

	TimeInterval period;
	World myworld;
	public TimeInterval getPeriod() {
		return period;
	}
	public void setPeriod(TimeInterval period) {
		this.period = period;
	}
	public World getMyworld() {
		return myworld;
	}
	public void setMyworld(World myworld) {
		this.myworld = myworld;
	}
	public EventGenerator(TimeInterval period, World myworld) {
		super();
		this.period = period;
		this.myworld = myworld;
	}
	/*
	 * generate a set of events
	 */
	public ArrayList<BayesEvent> generateEvents(int numberEvents){
		ArrayList<BayesEvent> result = new ArrayList<BayesEvent>();
		result.ensureCapacity(numberEvents);
		for(int i = 0; i < numberEvents; ++i)
			result.add(this.generateOneEvent());
		log.info("events generated: {}", result);
		return result;
	}
	/*
	 * This function generate a single event by sampling 
	 * - start time uniformly on the interval
	 * - duration according to lognormal(0,0.25) * 5% period duration
	 * - affected area as a square box 
	 * --- with edge length sampled by lognormal(0,0.25) * 5% world sizes
	 * --- with center uniformly sampled in the inner square of size 0.9*0.9;
	 */
	public BayesEvent generateOneEvent(){
		//Generate Type
		int typeInd = MyRandom.nextInt(TYPEVALUES.size()-3)+3;
		BayesEventType type = TYPEVALUES.get(typeInd);
		
		//Generate Location
		// TODO: improve to http://stackoverflow.com/questions/11178414/algorithm-to-generate-equally-distributed-points-in-a-polygon
		// http://tsusiatsoftware.net/jts/javadoc/com/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder.html
		double squareWorldEdgeLength=this.myworld.getLength()/4; //only work for square
		double Xcenter = MyRandom.nextDouble(0.1*squareWorldEdgeLength, 0.9*squareWorldEdgeLength) ;
		double Ycenter = MyRandom.nextDouble(0.1*squareWorldEdgeLength, 0.9*squareWorldEdgeLength) ;
		double gaussianRandSample=MyRandom.nextGaussian();
		double squareEventHalfEdgeLength= Math.exp(gaussianRandSample*0.25) * 0.025*squareWorldEdgeLength;
		Coordinate[] points = { 
			    new Coordinate(Xcenter-squareEventHalfEdgeLength, Ycenter-squareEventHalfEdgeLength),
			    new Coordinate(Xcenter-squareEventHalfEdgeLength, Ycenter+squareEventHalfEdgeLength),
			    new Coordinate(Xcenter+squareEventHalfEdgeLength, Ycenter+squareEventHalfEdgeLength),
			    new Coordinate(Xcenter+squareEventHalfEdgeLength, Ycenter-squareEventHalfEdgeLength),
			    new Coordinate(Xcenter-squareEventHalfEdgeLength, Ycenter-squareEventHalfEdgeLength)
			};
		GeometryFactory myGeometryFactory = myworld.getFactory();
		//Area location = (Area) myGeometryFactory.createPolygon(points);
		//System.out.println(points);
		LinearRing myLinearRing= myGeometryFactory.createLinearRing(points);
		LinearRing[] holes={};
		Area location = new Area(myLinearRing,holes,myGeometryFactory);

		
		//Generate Interval
		double randomDouble = MyRandom.nextDouble();
		long delayStart = (long) (randomDouble * period.toDurationMillis());
		DateTime startTime= period.getStart().plus(delayStart);
		long durationMilli = (long) (Math.exp(MyRandom.nextGaussian()*0.25) * 0.05*period.toDurationMillis());
		Duration dur = new Duration(durationMilli);
		TimeInterval interval = new TimeInterval(startTime,dur);
		
		return new BayesEvent(location, interval, type);
	}
}
