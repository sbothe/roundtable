package de.fhg.insight.roundTable.Bayes;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.joda.time.ReadableInstant;

import de.fhg.insight.roundTable.Bayes.Model.IModelIndep;


/**
*
* @author fschnitzler
* this Bayes Agent works on well-defined cells. It has a model for every cell, and is able to reason based on this 
* furthermore, it assumes independence of the different data
*/
public class BayesAgentCell<TroundTable> extends BaseBayesAgent<TroundTable>{
	//keep a list of cells
	//TODO: use RTree for improved efficiency
	//TODO: use geometryCollection
	protected Vector<Area> agentCells = null;
	protected Vector<IModelIndep> agentModels = null;
	private Vector<LinkedList<Integer>> participating_atRelevantcells = new Vector<LinkedList<Integer>>();
	int debug = 1;
	static int nextRTID=0;
	
	public BayesAgentCell(Vector<Area> agentCells,Vector<IModelIndep> agentModels) {
		super();
		this.setAgentCells(agentCells);
		this.setAgentModels(agentModels);
	}

	public BayesAgentCell() {
		super();
	}

	public BayesAgentCell(String ID) {
		super(ID);
	}

	//get data as a vector where each element correspond to a cell
	public void setData(Vector<Double> newData, ReadableInstant time){
		log.debug("agent {}, time {} receives {}",this.getID(), time ,newData);
		
		this.storeData(newData,time);
		
		//TODO: allow non temporally independent model
		//for every cell in roundTable, prepare data
		Vector<BayesRoundTableData> cachedRTD = new Vector<BayesRoundTableData>(this.agentCells.size());
		cachedRTD.setSize(this.agentCells.size());
		Vector<Boolean> hasBeenMatchedToRT = new Vector<Boolean>(this.agentCells.size());
		for(int cellInd = 0; cellInd < this.agentCells.size(); ++cellInd)
			hasBeenMatchedToRT.add(false);
		for(int tableInd = 0; tableInd < this.getTableNumber(); ++tableInd){
			LinkedList<Integer> relevantCellsCurrentTable = this.participating_atRelevantcells.get(tableInd);
			TroundTable table = this.participating_at.get(tableInd);
			for(int relevantCellInd = 0; relevantCellInd < relevantCellsCurrentTable.size(); ++relevantCellInd){
				int cellInd = relevantCellsCurrentTable.get(relevantCellInd);
				//if not cached, compute
				if(cachedRTD.get(cellInd)==null)
					cachedRTD.set(cellInd, this.computeCellRTD(cellInd,time,newData.get(cellInd)));
				//send data to RT
				this.addData(cachedRTD.get(cellInd), table);
				//remember this cell is linked to a RT
				hasBeenMatchedToRT.set(cellInd, true);
			}
		}
		
		
		
		//detect new anomaly
		int nbrNewAnomalies = 0;
		for(int cellInd = 0; cellInd < hasBeenMatchedToRT.size(); ++cellInd){
			if (! hasBeenMatchedToRT.get(cellInd)){
				if(this.isAnomaly(cellInd,time,newData.get(cellInd))){
					this.SignalData(this.computeCellRTD(cellInd,time,newData.get(cellInd)));
					++nbrNewAnomalies;
				}
			}
		}
		log.debug("agent {}, {} anomalies",this.getID(), nbrNewAnomalies );
	}
	
	private boolean isAnomaly(int cellInd, ReadableInstant time, Double data) {
		return this.agentModels.get(cellInd).isAnomaly(data, time);
	}

	public BayesRoundTableData computeCellRTD(int cellInd, ReadableInstant time, Double data){
		OntologyDistribution ontologyDistribution = this.agentModels.get(cellInd).computeOntologyDistribution(data, time);
		return new BayesRoundTableData(++nextRTID,this.agentCells.get(cellInd),new TimeInterval(time,time),ontologyDistribution,1.0);
	}

	//match a query to cells
	// This method returns the index of all the cells matching the area of the query.
	public LinkedList<Integer> matchQueryToCells(BayesEvent query) {
		return this.matchQueryToCells(query.getArea());
	}
	public LinkedList<Integer> matchQueryToCells(Area queryArea) {
		LinkedList<Integer> matches = new LinkedList<Integer>();
		for(int cellInd = 0; cellInd< this.agentCells.size(); ++cellInd){
			if (! queryArea.disjoint(this.agentCells.get(cellInd)))
				matches.add(cellInd);
		}
		return matches;
	}
	
	//store data
	public void storeData(Vector<Double> newData, ReadableInstant time){
		//TODO
	}
	
	//access data
	public SetOfDataVectors recoverData(TimeInterval timeInterval){
		return null;
		//TODO
	}

	
	public Vector<Area> getAgentCells() {
		return agentCells;
	}

	@Override
	public void collectInformation(BayesEvent query) {
		// TODO Auto-generated method stub
		//collect and analyze information for the relevant time and location
		
	}

	@Override
	public void cardsOn() {
		// TODO Auto-generated method stub. At the moments, data is sent directly to the RT, so nothing specific to do.
	}

	@Override
	public void joinTable(TroundTable t, Area area,
			ReadableInstant startTime) {
		super.joinTable(t, area, startTime);
		//compute list of relevant cells, do it only once
		this.participating_atRelevantcells.add(this.matchQueryToCells(area));
		//TODO: compute information from the beginning of the RT and send them, if not online monitoring.
	}
	@Override
	public void updateArea(TroundTable bayesRoundTable, Area searchArea){
		int tableIndex = this.getInternalRoundTableIndex(bayesRoundTable);
		this.participating_atRelevantcells.set(tableIndex,this.matchQueryToCells(searchArea));
	}

	@Override
	public int leaveTable(TroundTable t) {
		int index = super.leaveTable(t);
		this.participating_atRelevantcells.remove(index);
		return index;
	}

	public void setAgentCells(Vector<Area> agentCells) {
		//check you are not changing number of cells
		if (this.agentCells==null || this.agentCells.size() == agentCells.size())
				this.agentCells = agentCells;
		else
			throw new RuntimeException("Attempting to change the number of cells in a BayesAgentCell");
	}
	public void setAgentModels(Vector<IModelIndep> agentModels) {
		//check you have the correct number of models
		if (this.agentCells.size() == agentModels.size())
				this.agentModels = agentModels;
		else
			throw new RuntimeException("Attempting to use an incorrect number of models in a BayesAgentCell");
	}
	
	//used to access the data
	private static class SetOfDataVectors {
		 Vector<Vector<Double> > data;
		 Vector<ReadableInstant> time;
	}
}

