package de.fhg.insight.roundTable.Bayes;

import java.util.EnumMap;
import java.util.Map;

//this class contains a set of event types and associated numerical quantification
public class OntologyDistribution extends EnumMap<BayesEventType,Double>{
	//TODO: add methods to reason on onthology
	/**
	 * 
	 */
	private static final long serialVersionUID = 7258962441983241028L;

	public OntologyDistribution() {
		super(BayesEventType.class);
	}
	public OntologyDistribution(Class<BayesEventType> keyType) {
		super(keyType);
	}

	public OntologyDistribution(EnumMap<BayesEventType, ? extends Double> m) {
		super(m);
	}

	public OntologyDistribution(Map<BayesEventType, ? extends Double> m) {
		super(m);
	}

	public double getLikelihoodEventType(BayesEventType type){
		return this.get(type);
	}

	public BayesEventType getMaxLikelihoodEventType(){
		OntologyDistribution.Entry<BayesEventType,Double> maxEntry = null;
		for (OntologyDistribution.Entry<BayesEventType,Double> entry : this.entrySet())
		{
		    if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
		    {
		        maxEntry = entry;
		    }
		}
		if (maxEntry==null)
			return null;
		else
			return maxEntry.getKey();
	}
	
	//TODO: get most-likely leaf, given an ontology and some threshold
}
