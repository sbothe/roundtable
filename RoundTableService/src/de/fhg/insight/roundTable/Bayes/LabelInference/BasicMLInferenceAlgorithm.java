package de.fhg.insight.roundTable.Bayes.LabelInference;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.Bayes.BayesEventType;
import de.fhg.insight.roundTable.Bayes.BayesRoundTable;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.OntologyDistribution;

public class BasicMLInferenceAlgorithm implements ILabelInferenceAlgorithm {
	final Logger log = LoggerFactory.getLogger(BasicMLInferenceAlgorithm.class);

	protected OntologyDistribution combinedData = null;

	@Override
	public void receiveData(BayesRoundTableData data) {
		log.debug("receiving {}",data);
		//iterate over all labels
		for (Map.Entry<BayesEventType,Double> entry : data.getEventTypeDistribution().entrySet()) {
			BayesEventType label = entry.getKey();
			Double likelihood = entry.getValue();
			
			if (!combinedData.containsKey(label)){
				combinedData.put(label, 0.0);
			}
			combinedData.put(label, combinedData.get(label)+Math.log(likelihood));
		}
	}

	@Override
	public OntologyDistribution infer() {
		OntologyDistribution expCombinedData = new OntologyDistribution();
		Double maxLogLikelihood =  combinedData.get(combinedData.getMaxLikelihoodEventType());
		for (Map.Entry<BayesEventType,Double> entry : combinedData.entrySet()) {
			BayesEventType label = entry.getKey();
			Double logLikelihood = entry.getValue();
			//change back to likelihoods and scale
			expCombinedData.put(label, Math.exp(logLikelihood - maxLogLikelihood));
		}
		log.debug("inference result {}",expCombinedData);
		return expCombinedData;
	}

	@Override
	public BayesEventType getBestGuess() {
		return combinedData.getMaxLikelihoodEventType();
	}

	@Override
	public void reset() {
		combinedData = new OntologyDistribution(BayesEventType.class);
	}

}
