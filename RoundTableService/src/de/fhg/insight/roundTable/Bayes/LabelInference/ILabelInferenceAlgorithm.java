package de.fhg.insight.roundTable.Bayes.LabelInference;

import de.fhg.insight.roundTable.Bayes.BayesEventType;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.OntologyDistribution;

//This is the interface used for inference algorithm in the roundTable
public interface ILabelInferenceAlgorithm {

	public void receiveData(BayesRoundTableData data);
	
	public OntologyDistribution infer();
	
	public BayesEventType getBestGuess();
	
	public void reset();
}
