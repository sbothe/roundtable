package de.fhg.insight.roundTable.Bayes;

import java.util.ArrayList;
import java.util.TreeMap;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.RoundTableData;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Interfaces.IRoundTableManager;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.IanswerToRTDispatcher;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RT2ISAemitter;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RToutputEmitter;
import de.fhg.insight.roundTable.Bayes.LabelInference.BasicMLInferenceAlgorithm;
import de.fhg.insight.roundTable.Bayes.communication.RToutputDefaultEmitter;
import de.fhg.insight.roundTable.Bayes.stream.BayesRtRtmProcess;

public class BayesRoundTableManager<TBayesAgent> implements IRoundTableManager<TBayesAgent>, IanswerToRTDispatcher<Long,TBayesAgent> {
	static Logger log = LoggerFactory.getLogger(BayesRoundTableManager.class);
	
	RT2ISAemitter<TBayesAgent> agentEmitter = null;
	RToutputEmitter outputEmitter = new RToutputDefaultEmitter();
	TreeMap<Long,IBayesRoundTable> activeRoundTablesMap = new TreeMap<Long,IBayesRoundTable>();
	ArrayList<TBayesAgent> activeSensorAgentList = new ArrayList<TBayesAgent>();
	//new RT config
	int maxNumberRT = 0;
	int minimumDurationOfEvent = 300000; //default: 5 minutes (in milliseconds)
	int timeWithoutSupportingDataBeforeClosing = 300000; //default: 5 minutes (in milliseconds)
	
	public BayesRoundTableManager(RT2ISAemitter<TBayesAgent> emitter) {
		super();
		this.agentEmitter = emitter;
	}

	@Override
	public IBayesRoundTable newAnomaly(BayesRoundTableData anomaly, TBayesAgent a) {
		// create new round table
		IBayesRoundTable newTable = this.createNewRoundTable(anomaly, a);
		//make sure there are not too many RT
		if (this.getMaxNumberRT() >0 && activeRoundTablesMap.size() > maxNumberRT){
			//clear one RT
			Long keyOldestTable = activeRoundTablesMap.firstKey();
			log.warn("killing RT {}, too many round tables (max {})", keyOldestTable, this.getMaxNumberRT());
			activeRoundTablesMap.get(keyOldestTable).setStatus(BayesRoundTableStatus.KILLED);
			this.killRoundTable(activeRoundTablesMap.get(keyOldestTable));
		}
		
		//add to map
		activeRoundTablesMap.put(newTable.getId(),newTable);
		
		// add agents
		this.addAgents(newTable);
		
		
		return newTable;
	}

	//TODO: emitter
	private void addAgents(IBayesRoundTable newTable) {
		for(TBayesAgent agent : activeSensorAgentList){
			newTable.addAgent(agent);
		}		
	}
	
	//TODO: emitter
	private IBayesRoundTable createNewRoundTable(BayesRoundTableData anomaly, TBayesAgent a){
		BayesRoundTable newTable = new BayesRoundTable(anomaly,a,this);
		newTable.setLabelInferenceAlgorithm(new BasicMLInferenceAlgorithm());
		try {
			newTable.setEmitter(this.agentEmitter.clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//configure
		newTable.setMinimumDurationOfEvent(this.getMinimumDurationOfEvent());
		newTable.setTimeWithoutSupportingDataBeforeClosing(this.getTimeWithoutSupportingDataBeforeClosing());
		
		return newTable;
	}

	@Override
	public boolean registerAgent(TBayesAgent a) {
		log.info("registering agent {}",a);
		return activeSensorAgentList.add(a);
	}

	//TODO: emitter
	@Override
	public void manageRoundTable() {
		// TODO Auto-generated method stub

	}

	//TODO: emitter
	@Override
	// Manage existing RT, for example detecting and fusing RTs.
	public void checkOverlaps() {
		//check overlaps
		//merge what should be merged

	}

	//TODO: emitter
	@Override
	public void killRoundTable(IBayesRoundTable bayesRoundTable) {
		bayesRoundTable.close();
		//TODO: do something better. Extract the results?
		activeRoundTablesMap.remove(bayesRoundTable.getId());
	}

	public void emitReport(Long roundTableID, Instant currentTime, BayesRoundTableData event, BayesRoundTableStatus roundTableStatus){
		this.outputEmitter.emitReport( roundTableID,  currentTime,  event,  roundTableStatus);
	}
	@Override
	public void giveAnswerToRT(BayesRoundTableData answer, Long table, TBayesAgent a) {
		if (this.activeRoundTablesMap.get(table) != null){
			this.activeRoundTablesMap.get(table).receiveData(answer, a);
		}
		else
			log.debug("did not deliver message {} to roundTable {}: table no longer alive",answer,table);
	}

	public int getMaxNumberRT() {
		return maxNumberRT;
	}

	public void setMaxNumberRT(int maxNumberRT) {
		this.maxNumberRT = maxNumberRT;
		log.info("Maximum number of RT set to {}", maxNumberRT);
	}

	public int getMinimumDurationOfEvent() {
		return minimumDurationOfEvent;
	}

	public void setMinimumDurationOfEvent(int minimumDurationOfEvent) {
		this.minimumDurationOfEvent = minimumDurationOfEvent;
		log.info("minimumDurationOfEvent investigated in the RT set to {}", minimumDurationOfEvent);
	}

	public int getTimeWithoutSupportingDataBeforeClosing() {
		return timeWithoutSupportingDataBeforeClosing;
	}

	public void setTimeWithoutSupportingDataBeforeClosing(
			int timeWithoutSupportingDataBeforeClosing) {
		this.timeWithoutSupportingDataBeforeClosing = timeWithoutSupportingDataBeforeClosing;
		log.info("timeWithoutSupportingDataBeforeClosing a RT investigation set to {}", timeWithoutSupportingDataBeforeClosing);
	}

	public RToutputEmitter getOutputEmitter() {
		return outputEmitter;
	}

	public void setOutputEmitter(RToutputEmitter outputEmitter) {
		this.outputEmitter = outputEmitter;
	}

}
