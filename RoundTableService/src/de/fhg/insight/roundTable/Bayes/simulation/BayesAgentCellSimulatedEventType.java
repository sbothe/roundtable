package de.fhg.insight.roundTable.Bayes.simulation;

import java.util.Vector;

import org.joda.time.Instant;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesEventType;
import de.fhg.insight.roundTable.Bayes.OntologyDistribution;
import de.fhg.insight.roundTable.Bayes.Model.IModelIndep;

//TODO: create a better class than OntologyDistribution
public class BayesAgentCellSimulatedEventType<TroundTable> extends BayesAgentCellSimulated<TroundTable>{

	Vector<OntologyDistribution> nbrEventTypesAffectingCells;

	public BayesAgentCellSimulatedEventType(Vector<Area> agentCells,
			Vector<IModelIndep> agentModels) {
		super(agentCells, agentModels);
	}
	public BayesAgentCellSimulatedEventType() {
		super();
	}
	
	public BayesAgentCellSimulatedEventType(String ID) {
		super(ID);
	}
	@Override
	protected Double sampleCell(int cellInd, Instant sampleTime) {
		return this.agentModels.get(cellInd).sample(sampleTime, this.nbrEventTypesAffectingCells.get(cellInd));
	}
	
	@Override
	protected void removeEventEffectToCell(int cellInd, int nextEventInd2) {
		OntologyDistribution cellOntologyDistribution = this.nbrEventTypesAffectingCells.get(cellInd);
		BayesEventType eventType = this.myEvents.get(nextEventInd2).getType();
		Double currentNumberEvent = cellOntologyDistribution.get(eventType);
		cellOntologyDistribution.put(eventType, currentNumberEvent-1);
		//TODO: do some cleanup to free memory
	}
	
	@Override
	protected void addEventEffectToCell(int cellInd, int nextEventInd2) {
		OntologyDistribution cellOntologyDistribution = this.nbrEventTypesAffectingCells.get(cellInd);
		//makes sure it exists
		if (cellOntologyDistribution == null){
			cellOntologyDistribution = new OntologyDistribution(BayesEventType.class);
			this.nbrEventTypesAffectingCells.set(cellInd,cellOntologyDistribution);
		}
		BayesEventType eventType = this.myEvents.get(nextEventInd2).getType();
		Double currentNumberEvent = cellOntologyDistribution.get(eventType);
		if (currentNumberEvent==null)
			currentNumberEvent=0.0;
		cellOntologyDistribution.put(eventType, currentNumberEvent+1);
	}
	@Override
	public void setAgentCells(Vector<Area> agentCells) {
		super.setAgentCells(agentCells);
		if(this.nbrEventTypesAffectingCells==null)
			this.nbrEventTypesAffectingCells = new Vector<OntologyDistribution>();
		this.nbrEventTypesAffectingCells.setSize(this.agentCells.size());
	}
	
}
