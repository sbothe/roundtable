package de.fhg.insight.roundTable.Bayes.simulation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.ReadableInstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesAgentCell;
import de.fhg.insight.roundTable.Bayes.BayesEvent;
import de.fhg.insight.roundTable.Bayes.BayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Model.IModelIndep;

public class BayesAgentCellSimulated<TroundTable> extends BayesAgentCell<TroundTable>{
	final Logger log = LoggerFactory.getLogger(BayesAgentCellSimulated.class);

	//necessary for the simulation
	ArrayList<BayesEvent> myEvents = null;
	Duration samplingPeriod = null;
	Instant lastSampledTime = null;
	NormalDistribution noiseDistribution = null;
	//caching cells affected by events
	int nextEventInd = 0;
	ArrayList<Integer> activeEvents = new ArrayList<Integer>();
	Vector<Integer> nbrEventsAffectingCells;
	ArrayList<LinkedList<Integer>> cellsAffectedByEvent = new ArrayList<LinkedList<Integer> >();

	public BayesAgentCellSimulated(Vector<Area> agentCells,
			Vector<IModelIndep> agentModels) {
		super(agentCells, agentModels);
		}
	
	public BayesAgentCellSimulated() {
		super();
	}

	public BayesAgentCellSimulated(String ID) {
		super(ID);
	}

	//simulate data up to @time, from the the last sample generated
	public void simulateAndAddData(ReadableInstant time){
		//sample the data
		while(lastSampledTime.isBefore(time)){
			//update cells affected by noise
			this.updateActiveEvents(lastSampledTime);
			//check if cells are affected by noise
			int cellNumbers = this.agentCells.size();
			Vector<Double> newData = new Vector<Double>(cellNumbers);
			newData.setSize(cellNumbers);
			for(int cellInd=0; cellInd < cellNumbers; ++cellInd){
				newData.set(cellInd,sampleCell(cellInd,lastSampledTime));
			}
			
			//add the data
			this.setData(newData, new Instant(lastSampledTime));
			lastSampledTime=lastSampledTime.plus(this.samplingPeriod);
		}
	}
	
	//TODO: move to derived class with next 2 methods
	protected Double sampleCell(int cellInd, Instant sampleTime) {
		boolean isCellAffected = (this.nbrEventsAffectingCells.get(cellInd)>0);
		if (! isCellAffected){
			return sampleCellStandard(cellInd,sampleTime);
		}
		else{
			 return sampleCellNoisy(cellInd,sampleTime);
		}
	}

	private Double sampleCellNoisy(int cellInd, ReadableInstant time) {
		// TODO Auto-generated method stub
		Double measure = this.sampleCellStandard(cellInd, time);
		Double noise = this.noiseDistribution.sample();
		return measure + noise;
	}

	private Double sampleCellStandard(int cellInd, ReadableInstant time) {
		// TODO Auto-generated method stub
		Double tmp = this.agentModels.get(cellInd).sample(time);
		return tmp;
	}

	//update the list of active events for time lastSampledTime2
	//assumes the event list are sorted by chronological start oder
	private void updateActiveEvents(Instant lastSampledTime2) {
		//remove old events
		for(int activeEventIndex = 0; activeEventIndex < this.activeEvents.size(); ++activeEventIndex){
			int eventIndex = this.activeEvents.get(activeEventIndex);
			log.info("currently active event " + this.myEvents.get(eventIndex).getInterval());
			if(! this.myEvents.get(eventIndex).getInterval().contains(lastSampledTime2)){
				//event no longer active, update
				LinkedList<Integer> cellsAffectedList = cellsAffectedByEvent.remove(activeEventIndex);
				for(Integer cellInd : cellsAffectedList) {
					nbrEventsAffectingCells.set(cellInd,nbrEventsAffectingCells.get(cellInd)-1);
					this.removeEventEffectToCell(cellInd,eventIndex);
				}
				this.activeEvents.remove(activeEventIndex);
				//because one element was removed
				--activeEventIndex;
			}
		}
		
		//add new events
		while(nextEventInd < this.myEvents.size()){
			BayesEvent nextEvent = this.myEvents.get(nextEventInd);
			if(! lastSampledTime2.isAfter(nextEvent.getInterval().getStart()))
				break;
			//do not update events that finish before current time
			else if(nextEvent.getInterval().contains(lastSampledTime2)){
				//get list of affected cells
				LinkedList<Integer> matches = this.matchQueryToCells(nextEvent.getArea());
				for(int matchedCellInd=0; matchedCellInd < matches.size(); ++matchedCellInd){
					int cellInd = matches.get(matchedCellInd);
					this.nbrEventsAffectingCells.set(cellInd, this.nbrEventsAffectingCells.get(cellInd)+1);
					this.addEventEffectToCell(cellInd,nextEventInd);
				}
				cellsAffectedByEvent.add(matches);
				this.activeEvents.add(nextEventInd);
			}
			++nextEventInd;
		}
		
	}

	//TODO: more to derive class with next method
	protected void removeEventEffectToCell(int cellInd, int nextEventInd2) {
	}
	
	protected void addEventEffectToCell(int cellInd, int nextEventInd2) {
	}

	public ArrayList<BayesEvent> getMyEvents() {
		return myEvents;
	}
	
	//assumes the event list are sorted by chronological start oder
	public void setMyEvents(ArrayList<BayesEvent> myEvents) {
		this.myEvents = myEvents;
		
		nextEventInd = 0;
		//TODO: init everything related to events
	}
	
	public Duration getSamplingPeriod() {
		return samplingPeriod;
	}

	public void setSamplingPeriod(Duration samplingPeriod) {
		 
		this.samplingPeriod = samplingPeriod;
	}

	public ReadableInstant getLastSampledTime() {
		return lastSampledTime;
	}

	public void setLastSampledTime(ReadableInstant lastSampledTime) {
		this.lastSampledTime = new Instant(lastSampledTime);
	}

	@Override
	public void setAgentCells(Vector<Area> agentCells) {
		super.setAgentCells(agentCells);
		if(nbrEventsAffectingCells==null)
			nbrEventsAffectingCells = new Vector<Integer>();
		nbrEventsAffectingCells.setSize(this.agentCells.size());
		for(int ind = 0; ind< this.agentCells.size(); ++ind)
			nbrEventsAffectingCells.set(ind,0);
	}

	public NormalDistribution getNoiseDistribution() {
		return noiseDistribution;
	}

	public void setNoiseDistribution(NormalDistribution noiseDistribution) {
		this.noiseDistribution = noiseDistribution;
	}
	
	public void setNoiseDistribution(double mean, double sd){
		this.noiseDistribution = new NormalDistribution(mean,sd);
	}
	
}
