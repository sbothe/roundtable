package de.fhg.insight.roundTable.Bayes.Interfaces;

import de.fhg.insight.roundTable.RoundTableData;
import de.fhg.insight.roundTable.Bayes.BayesEvent;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableStatus;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.RT2ISAemitter;

public interface IBayesRoundTable<TBayesAgent> {
	/*** Interface with the round table manager ***/
	
	/*
	 * add agent to the round table
	 */
	public boolean addAgent(TBayesAgent a);
	
	/*
	 * remove agent from the round table
	 */
	public boolean removeAgent(TBayesAgent a);

	/*
	 * discuss data received and reach a conclusion
	 */
	public BayesRoundTableData discuss();
	
	/*
	 * output the most recent conclusion
	 */
	public BayesEvent getExplanation();

	/*** Interface with the ISAs ***/

	/*
	 * send or update requests to agents based on the most recent conclusion
	 */
	public void updateRequestsToagents();
	
	/*
	 * receives a data item from an agent and store it
	 */
	public void receiveData(BayesRoundTableData data, TBayesAgent a);
	
	/*
	 * specifies the dataItem that must be investigate
	 * The round table must always contain this issue
	 */
	public void setIssue(BayesRoundTableData rtData) throws CloneNotSupportedException;
	
	/*
	 * provide the dataItem investigated
	 */
	public BayesRoundTableData getIssue();

	public void setEmitter(RT2ISAemitter<TBayesAgent> clone);
	
	/*** general ***/
	
	//prepare the roundTable to be destroyed
	public void close();
	
	public Long getId();
	
	public void setStatus(BayesRoundTableStatus newStatus);
}
