package de.fhg.insight.roundTable.Bayes.Interfaces.Internal;

import org.joda.time.DateTime;
import org.joda.time.ReadableInstant;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesRoundTable;

public interface RT2ISAemitter<TBayesAgent> extends Cloneable {
	public void tellAgentToUpdateSearch(TBayesAgent participatingAgent,
			IBayesRoundTable<TBayesAgent> bayesRoundTable, Area searchArea2);

	public void tellAgentToJoin(TBayesAgent a, BayesRoundTable<TBayesAgent> table, Area area, DateTime startTime);

	public void tellAgentToLeave(TBayesAgent a,
			IBayesRoundTable<TBayesAgent> bayesRoundTable);

	public RT2ISAemitter<TBayesAgent> clone() throws CloneNotSupportedException;
}
