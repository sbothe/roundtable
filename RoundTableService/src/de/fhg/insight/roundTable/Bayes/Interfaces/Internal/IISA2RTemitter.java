package de.fhg.insight.roundTable.Bayes.Interfaces.Internal;

import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesAgent;

/*
 * @author fschnitzler
 * 
 * The goal of this interface is to allow a specialization of the communication of the ISA depending on the context, for example batch system or streaming agent.
 */

public interface IISA2RTemitter<TroundTable> {
	
	/*** from the ISA to the RT ***/
	/*
	 * This method sends a particular @data to a given round @table. @sensor is the ISA which generated the data.
	 */
	public void addData(BayesRoundTableData data, TroundTable table, IBayesAgent<TroundTable> sensor);
	
	/*
	 * This method reports on a new @anomalousData. @sensor is the ISA which generated the data.
	 */
	public void newAnomaly(BayesRoundTableData anomalousData, IBayesAgent<TroundTable> sensor);

}
