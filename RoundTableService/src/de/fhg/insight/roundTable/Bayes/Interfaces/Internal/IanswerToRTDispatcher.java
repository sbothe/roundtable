package de.fhg.insight.roundTable.Bayes.Interfaces.Internal;

import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;

public interface IanswerToRTDispatcher<TroundTable,TBayesAgent> {
	public void giveAnswerToRT(BayesRoundTableData answer, TroundTable table, TBayesAgent a);
}
