package de.fhg.insight.roundTable.Bayes.Interfaces.Internal;

import org.joda.time.Instant;

import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableStatus;

/*
 * @author fschnitzler
 * 
 * The goal of this interface is to allow a specialization of the communication of 
 * the RT to the next component in the system, for example batch or streaming system.
 */


public interface RToutputEmitter {
	/* 
	 * Transmit a round table report to the next component in the system
	 */
	void emitReport(Long roundTableID, Instant currentTime, BayesRoundTableData event, BayesRoundTableStatus roundTableStatus);
}
