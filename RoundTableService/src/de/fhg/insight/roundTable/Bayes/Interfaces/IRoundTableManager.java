package de.fhg.insight.roundTable.Bayes.Interfaces;

import org.joda.time.Instant;

import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableStatus;

//TODO: use an emitter for the RT as well
public interface IRoundTableManager<TBayesAgent> {
	/*
	 * process a new anomaly signed by an ISA, for example by creating a round table
	 */
	public IBayesRoundTable newAnomaly(BayesRoundTableData anomaly, TBayesAgent a);
	
	/*
	 * register the existence of a new agent
	 */
	public boolean registerAgent(TBayesAgent a);
	
	/*
	 * Check what the round tables are doing
	 */
	public void manageRoundTable();
	
	/*
	 * communicate output from a roundTable
	 */
	public void emitReport(Long roundTableID, Instant currentTime, BayesRoundTableData event, BayesRoundTableStatus roundTableStatus);
	
	/*
	 * Check what results are available in the round table
	 * Take appropriate actions
	 */
//	public void checkResults();
	
	/*
	 * Check whether any round tables overlap
	 * Take appropriate actions
	 */
	public void checkOverlaps();

	public void killRoundTable(IBayesRoundTable bayesRoundTable);
}
