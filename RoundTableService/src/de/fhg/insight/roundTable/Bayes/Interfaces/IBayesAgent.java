package de.fhg.insight.roundTable.Bayes.Interfaces;

import org.joda.time.ReadableInstant;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesEvent;
import de.fhg.insight.roundTable.Bayes.BayesRoundTable;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableData;
import de.fhg.insight.roundTable.Bayes.Interfaces.Internal.IISA2RTemitter;



/**
 * Interface for ...
 * 
 * @author abablok
 * @author sbothe
 * @author fschnitzler
 * 
 */
public interface IBayesAgent<TroundTable> {
	/*** Off-Line processing interface (High-level analytics) ***/
	
	/**
	 * Collect Information on the given Event, i.e. search for related Data
	 */
	void collectInformation(BayesEvent query);

	/*** On-Line processing interface (round tables) ***/
	
	/*
	 * Signal an anomalous data item not connected to any specific round table
	 * @param d
	 */
	public void SignalData(BayesRoundTableData answer);
	
	/*
	 * Put Data for discussion on all relevant round tables
	 * calls 
	 */
	void cardsOn();
	
	/*
	 * Add a data item to a specific round table
	 * @param d
	 */
	public void addData(BayesRoundTableData answer, TroundTable table);

	/*** This part of the interface relates to information and inner functions of the agent ***/
	
	/*
	 * Tells an agent he is to report to the RT any information within area, starting from time.
	 * 
	 * @param t : reoundTable
	 * @param area : area to be monitored
	 * @param startTime : start of the monitoring
	 */
	public void joinTable(TroundTable t, Area area, ReadableInstant startTime);

	/**
	 * Provides some information about the Agent, to help the RT manager and the RT to best deal with the data
	 * @return
	 */
	public String getID();
	public void setId(String agentID);

	/**
	 * Bla...
	 * 
	 */
	public int leaveTable(TroundTable table);

	public void updateArea(TroundTable bayesRoundTable, Area searchArea);

	public void setISA2RTemitter(IISA2RTemitter<TroundTable> iSA2RTemitter);
	/**
	 * @param someData
	 * @param t
	 */
	//public void callTable(BayesRoundTableData someData, BayesRoundTable table);
	
	//TODO: update table time or location
}