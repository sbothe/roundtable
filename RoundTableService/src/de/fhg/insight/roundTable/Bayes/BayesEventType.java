package de.fhg.insight.roundTable.Bayes;

//TODO: use 
// import apache.commons.lang3.builder.HashCodeBuilder
// TODO: build from ontolofy

// Indentification for an event. node of the onthology
public enum BayesEventType {
	ROOT, 		//lvl 0
	NORMAL, 	//lvl 1
	ANOMALY, 
	FLOOD,	 	//lvl 2
	FIRE,
	SNOW,
	TRAFFIC_JAM;
}
