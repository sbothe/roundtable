package de.fhg.insight.roundTable.Bayes;

import java.io.IOException;

import org.geotools.geojson.geom.GeometryJSON;

import net.minidev.json.JSONAware;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.util.AffineTransformation;

//TODO. To be used with JSON-smart, has to implement JSONAware. Not clear how it can decode.
//TODO. Other possibilities: use GeoTools
public class Area extends com.vividsolutions.jts.geom.Polygon implements JSONAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2464567457259217260L;

	public Area(LinearRing shell, LinearRing[] holes, GeometryFactory factory) {
		super(shell, holes, factory);
		// TODO Auto-generated constructor stub
	}

	public Area(LinearRing shell, LinearRing[] holes,
			PrecisionModel precisionModel, int SRID) {
		super(shell, holes, precisionModel, SRID);
		// TODO Auto-generated constructor stub
	}

	public Area(LinearRing shell, PrecisionModel precisionModel, int SRID) {
		super(shell, precisionModel, SRID);
		// TODO Auto-generated constructor stub
	}

	public Area(Geometry intersection) {
		super(intersection.getFactory().createLinearRing(intersection.getCoordinates()),
				new LinearRing[0],
				intersection.getFactory());
	}
	
	//Scale the Area with respect to its centroid.
	public Area scale(double factor){
		AffineTransformation affineTransformation = new AffineTransformation();
		Point center = this.getCentroid();
		affineTransformation.setToScale(factor, factor);
		AffineTransformation translation = new AffineTransformation();
		translation.setToTranslation(center.getX() * (1-factor), center.getY() * (1-factor));
		affineTransformation.compose(translation);
		return new Area(affineTransformation.transform(this));
	}
	
	//Create a rectangle
	//TODO: place in a better area
	public Area makeRectangle(double xSmall, double xBig, double ySmall, double yBig) {
		Coordinate[] points = {
			    new Coordinate(xSmall, ySmall),
			    new Coordinate(xSmall, yBig),
			    new Coordinate(xBig, yBig),
			    new Coordinate(xBig, ySmall),
			    new Coordinate(xSmall, ySmall)
			};
		GeometryFactory myGeometryFactory = this.getFactory();
		LinearRing myLinearRing= myGeometryFactory.createLinearRing(points);
		LinearRing[] holes={};
		return new Area(myLinearRing,holes,myGeometryFactory);
	}

	@Override
	public String toJSONString() {
		GeometryJSON g = new GeometryJSON();
        return g.toString(this) ;
	}
	public static Area createFromJSON(String areaJSON){
		GeometryJSON g = new GeometryJSON();
		Geometry recovered = null;
		try {
			recovered = g.read(areaJSON);
		} catch (IOException e) {
			//log.eror("could not create world from {}", worldJSON);
			e.printStackTrace();
		}
		return new Area(recovered); 
	}
}
