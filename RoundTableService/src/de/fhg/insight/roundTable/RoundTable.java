package de.fhg.insight.roundTable;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * 
 * @author abablok
 * @author sbothe
 * 
 */
public class RoundTable implements IRoundTable {

	private RoundTableData issue = null;
	private final LinkedList<IIsaAgent> participants = new LinkedList<>();
	private final HashMap<IIsaAgent, IRoundTableData> OnTableData = new HashMap<>();
	private RoundTableData result = null;

	// private TreeSet<Calendar> data_times = new TreeSet<Calendar>;

	// -----Konstruktor-----
	public RoundTable(IIsaAgent initiator) {
		this.setIssue(initiator.getData(null));

	}

	// -------Methods-------

	// add an Agent to the Table
	@Override
	public boolean addAgent(IIsaAgent a) {
		return ((this.participants.add(a)));
	}

	public void addData(IIsaAgent a, IRoundTableData d) {
		OnTableData.put(a, d);
	}

	// set the Issue to discuss based on given data
	private void setIssue(RoundTableData rtData) {
		if (this.issue == null) {
			this.issue = rtData;
			try {
				this.result = (RoundTableData) rtData.clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out
					.println("You are not allowed to set a new Issue (because we are just discussing one)");
		}
	}

	public RoundTableData getIssue() {
		return this.issue;
	}

	// discuss the information provided by the agents
	// At the moment: the round table uses the best semantic information
	@Override
	public IRoundTableData discuss() {
		for (IIsaAgent a : participants) {
			this.addData(a, a.getData(issue));

		}

		for (IRoundTableData d : OnTableData.values()) {
			if (!(d == null)) {
				if (d.getRange_event() < result.getRange_event()) {
					result.setEvent(d.getEvent());
				}
			}
		}

		return result;

	}

	@Deprecated
	private void DataOnTable() {
		System.out.println("What lies on table: ");
		for (IRoundTableAgent a : OnTableData.keySet()) {
			try {
				System.out.println(OnTableData.get(a).toString());
			} catch (NullPointerException e) {
				System.out.println("Nothing");
			}
		}

	}

	@Deprecated
	private void result() {
		System.out.println("The result of the discussion is "
				+ result.toString());
	}
}