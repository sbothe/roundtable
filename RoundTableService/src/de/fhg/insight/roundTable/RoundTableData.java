package de.fhg.insight.roundTable;

import java.util.Calendar;
import java.util.GregorianCalendar;

import de.fhg.insight.roundTable.utils.DistanceUtils;

/**
 * 
 * @author abablok
 * @author sbothe
 * 
 */
public abstract class RoundTableData extends IRoundTableData implements
		Cloneable {
	// TODO: Fix appropriate information abstraction, confirm only needed ones
	// go here :-/

	/**
	 * spatial component of the tuple
	 */
	private int location;

	public int getLocation() {
		return location;
	}

	/**
	 * ,,semantic'' component of the tuple
	 */
	private int event;

	public void setEvent(int event) {
		this.event = event;
	}

	public int getEvent() {
		return event;
	}

	private final int range_event = 0;

	public int getRange_event() {
		return range_event;
	}

	/**
	 * temporal component of the tuple
	 */
	private final Calendar date = new GregorianCalendar(1900, Calendar.JANUARY,
			1);

	public Calendar getDate() {
		return date;
	}

	private final int range_date = 0;

	private double relevance;
	private double accuracy;

	public double getAccuracy() {
		return accuracy;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		return null;
	}

	public double distance(RoundTableData reference) {
		return DistanceUtils.euklidian(
				this.getLocation() - reference.getLocation(), this.getEvent()
						- reference.getEvent(), this.getDate()
						.getTimeInMillis()
						- reference.getDate().getTimeInMillis());
	}
}
