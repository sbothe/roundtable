package de.fhg.insight.roundTable;

public interface IIsaAgent {

	/**
	 * The ISA is expected to provide associated information from its local data
	 * model.
	 * 
	 * @param reference
	 *            information point
	 * @return the associated information from the ISA
	 */
	public RoundTableData getData(RoundTableData reference);

}
