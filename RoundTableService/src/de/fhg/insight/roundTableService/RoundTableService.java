package de.fhg.insight.roundTableService;

import java.util.HashSet;
import java.util.Set;

import de.fhg.insight.roundTable.IIsaAgent;
import de.fhg.insight.roundTable.IRoundTable;
import de.fhg.insight.roundTable.RoundTable;
import de.fhg.insight.roundTable.RoundTableData;

public final class RoundTableService {
	private static Set<IIsaAgent> isas = new HashSet<>();

	private RoundTableService() {
		// Make this a static thing..
	}

	/**
	 * Register an isa at the RoundTableService
	 * 
	 * @param isa
	 *            the agent to be added.
	 */
	public static void registerAgent(IIsaAgent isa) {

		RoundTableService.isas.add(isa);
	}

	/**
	 * @return
	 * @throws CloneNotSupportedException
	 */
	public static IRoundTable createRoundTable(IIsaAgent initiator) {
		RoundTableData issue = initiator.getData(null);
		RoundTable table = new RoundTable(initiator);
		assignParticipants(table);
		return table;
	}

	private static void assignParticipants(RoundTable table) {
		// TODO don't assign all isas to each rt, do sth more sophisticated here
		for (IIsaAgent i : isas) {
			table.addAgent(i);
		}
	}
}
