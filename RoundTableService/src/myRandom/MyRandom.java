package myRandom;

import java.util.Random;

public class MyRandom {
	public static Random generator = new Random();

	
	public static void setSeed(long seed) {
		// TODO Auto-generated method stub
		generator.setSeed(seed);
	}

	
	public static void nextBytes(byte[] bytes) {
		// TODO Auto-generated method stub
		generator.nextBytes(bytes);
	}

	
	public static int nextInt() {
		// TODO Auto-generated method stub
		return generator.nextInt();
	}

	
	public static int nextInt(int n) {
		// TODO Auto-generated method stub
		return generator.nextInt(n);
	}

	
	public static long nextLong() {
		// TODO Auto-generated method stub
		return generator.nextLong();
	}

	
	public static boolean nextBoolean() {
		// TODO Auto-generated method stub
		return generator.nextBoolean();
	}

	
	public static float nextFloat() {
		// TODO Auto-generated method stub
		return generator.nextFloat();
	}

	
	public static double nextDouble() {
		// TODO Auto-generated method stub
		return generator.nextDouble();
	}

	public static double nextDouble(double start, double end) {
		return start+ MyRandom.nextDouble() * (end-start);
	}
	
	public static double nextDouble(double end) {
		return MyRandom.nextDouble() * (end);
	}
	
	public static double nextGaussian() {
		// TODO Auto-generated method stub
		return generator.nextGaussian();
	}	
}
