package de.fhg.insight.roundTable.Tests;

import java.util.Vector;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;

import de.fhg.insight.roundTable.Bayes.Area;

public class CellsConstructor {
	Area origPolygon;
	Envelope envelope;

	public CellsConstructor(Area origPolygon) {
		super();
		this.origPolygon = origPolygon;
		this.envelope = this.origPolygon.getEnvelopeInternal() ;
	}
	//create a lattice out of envelope by dividing each x and y length by subdiv
	public Vector<Area> makeLattice(int subdiv){
		Vector<Area> lattice = new Vector<Area>(subdiv*subdiv);
		double stepX = (envelope.getMaxX() - envelope.getMinX())/subdiv;
		double stepY = (envelope.getMaxY() - envelope.getMinY())/subdiv;
		for(int xInd = 0; xInd < subdiv; ++ xInd){
			for(int yInd = 0; yInd < subdiv; ++ yInd){
				//create base area
				double xSmall = envelope.getMinX() + xInd*stepX;
				double xBig = envelope.getMinX() + (1+xInd)*stepX;
				double ySmall = envelope.getMinY() + yInd*stepY;
				double yBig = envelope.getMinY()+ (1+yInd)*stepY;
				Coordinate[] points = {
					    new Coordinate(xSmall, ySmall),
					    new Coordinate(xSmall, yBig),
					    new Coordinate(xBig, yBig),
					    new Coordinate(xBig, ySmall),
					    new Coordinate(xSmall, ySmall)
					};
				GeometryFactory myGeometryFactory = origPolygon.getFactory();
				LinearRing myLinearRing= myGeometryFactory.createLinearRing(points);
				LinearRing[] holes={};
				Area newArea = new Area(myLinearRing,holes,myGeometryFactory);
				
				//intersection
				newArea = new Area(newArea.intersection(origPolygon));
				//do not add point or lines
				if(newArea.getDimension()>1)
					lattice.add(newArea);
			}
		}
		return lattice;
	}
}
