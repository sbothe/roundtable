package de.fhg.insight.roundTable.Tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;

import de.fhg.insight.roundTable.IIsaAgent;
import de.fhg.insight.roundTable.IRoundTable;
import de.fhg.insight.roundTable.IRoundTableAgent;
import de.fhg.insight.roundTable.IRoundTableData;
import de.fhg.insight.roundTable.Samples.Constants;
import de.fhg.insight.roundTable.Samples.RoundTableDataExample;
import de.fhg.insight.roundTable.Samples.TestAgent;
import de.fhg.insight.roundTableService.RoundTableService;

/**
 * 
 * @author abablok
 * @author sbothe
 * 
 */
public class TestRoundTable extends IRoundTableData {

	@Test
	public void simpleRoundTableSessionTest() {
		// Have a RT..
		IRoundTable t = null;

		printDebug();

		// TODO: exchange sysout to logger
		System.out.println("\n#############");

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());

		for (int i = 0; i < 3; i++) {

			TestAgent a = new TestAgent(); // instantiate test agent
			if (i == 0) {
				t = RoundTableService.createRoundTable((IIsaAgent) a);
				initiateRandom(t);
			}
			// every agent get a data point
			for (int j = 0; j < 1; j++) {
				c.add(Calendar.HOUR, (int) (Math.random() * 8 - 4));
				RoundTableDataExample issue = new RoundTableDataExample(
						sdf.format(c.getTime()),
						(int) ((Math.random() * 9) - 2),
						(int) (Math.random() * 6 - 1));
				a.addData(issue);
				System.out.print("\n");
			}
			// agents are joined to the table
			a.joinTable(t);
			c.setTime(new Date());
		}
		IRoundTableData result = t.discuss();

	}

	private static void printDebug() {
		String[] possibleLoc = Constants.possibleLoc;
		String[] possibleEvent = Constants.possibleEvent;

		System.out.println("possible locations are:");
		for (String element : possibleLoc) {
			System.out.print(element + " ");
		}
		System.out.println("\n");
		for (String element : possibleEvent) {
			System.out.print(element + " ");
		}
		System.out.println("\n");
	}

	// Move this to factory or sth like that..
	static private void initiateRandom(IRoundTable t) {

		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		RoundTableDataExample issue = new RoundTableDataExample(sdf.format(c
				.getTime()), (int) (Math.random() * 10 - 1), -1);

		IRoundTableAgent a = new TestAgent(issue, "initiator"); // create new
																// data point
		a.callTable(issue, t);
	}
}
