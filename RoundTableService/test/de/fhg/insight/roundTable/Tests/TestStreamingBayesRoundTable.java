package de.fhg.insight.roundTable.Tests;

import static org.junit.Assert.fail;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestStreamingBayesRoundTable {
	
	@Test
	public void test() {

		final Logger log = LoggerFactory.getLogger(TestStreamingBayesRoundTable.class);

		try {
			URL url = TestStreamingBayesRoundTable.class.getResource("/TestStreamingBayesRoundTableLocalConfig.xml");
			System.out.println(url);
			log.info("Starting container from {}", url);

			stream.run.main(url);
		} catch (Exception e) {
			log.error("Error message: {}", e.getMessage());
			e.printStackTrace();
			fail("Test failed: " + e.getMessage());
		}
	}
}
