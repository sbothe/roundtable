package de.fhg.insight.roundTable.Tests;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import org.geotools.geojson.geom.GeometryJSON;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.expressions.ExpressionResolver;
import stream.io.JSONStream;
import stream.io.SourceURL;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;

import de.fhg.insight.roundTable.Bayes.Area;
import de.fhg.insight.roundTable.Bayes.BayesEvent;
import de.fhg.insight.roundTable.Bayes.BayesRoundTableManager;
import de.fhg.insight.roundTable.Bayes.EventGenerator;
import de.fhg.insight.roundTable.Bayes.TimeInterval;
import de.fhg.insight.roundTable.Bayes.World;
import de.fhg.insight.roundTable.Bayes.Interfaces.IBayesRoundTable;
import de.fhg.insight.roundTable.Bayes.Model.IModelIndep;
import de.fhg.insight.roundTable.Bayes.Model.ModelIndepEventType;
import de.fhg.insight.roundTable.Bayes.Model.ModelIndepFactory;
import de.fhg.insight.roundTable.Bayes.Model.ModelIndepGaussian;
import de.fhg.insight.roundTable.Bayes.communication.ISA2RTBatchEmitter;
import de.fhg.insight.roundTable.Bayes.communication.RT2ISABatchEmitter;
import de.fhg.insight.roundTable.Bayes.simulation.BayesAgentCellSimulated;
import de.fhg.insight.roundTable.Bayes.simulation.BayesAgentCellSimulatedEventType;
import de.fhg.insight.roundTable.Bayes.utils.JSONHelper;

/**
 * 
 * @author Francois
 * 
 */
public class TestBayesRoundTable {
	
	@Test
	public void simpleRoundTableSessionTest() {
		final Logger log = LoggerFactory.getLogger(TestStreamingBayesRoundTable.class);
		//*****************************************
		//initiate random
		Random generator=initiateRandom();
		
		//*****************************************
		// Generate World : square of size dim
		double dim=1.0;
		Coordinate[] points = { 
			    new Coordinate(0.0,0.0),
			    new Coordinate(dim,0.0),
			    new Coordinate(dim,dim),
			    new Coordinate(0.0,dim),
			    new Coordinate(0.0,0.0),
			};
		GeometryFactory myGeometryFactory = new GeometryFactory();
		LinearRing myLinearRing= myGeometryFactory.createLinearRing(points);
		World myWorld = new World(myLinearRing,myGeometryFactory.getPrecisionModel(),0);
		
		log.info("The world is {} ", myWorld);
		
		//*****************************************
		//Generate a set of events in the world
		DateTime startTime= new DateTime(2013, 01, 01, 0, 0); //y/m/d/h/m
		DateTime endTime= new DateTime(2013, 01, 01, 23, 59);
		TimeInterval systemTimeInterval= new TimeInterval(startTime,endTime);
		EventGenerator myEventGenerator = new EventGenerator(systemTimeInterval, myWorld);
		
		int nbrEvents =1;
		ArrayList<BayesEvent> myEvents= myEventGenerator.generateEvents(nbrEvents);
		Collections.sort(myEvents, BayesEvent.BayesEventStartTimeComparator);
		System.out.println(myEvents);
		
		//*****************************************
		// Generate a RT manager
		RT2ISABatchEmitter RTemitter = new RT2ISABatchEmitter();
		BayesRoundTableManager roundTableManager = new BayesRoundTableManager(RTemitter);
		
		//*****************************************
		//***** Generate a set of ISAs
		//***************
		// Twitter-like
		// Twitter: data config cells and models
		Vector<Area> twitterAgentCells = new Vector<Area>();
		twitterAgentCells.add(myWorld);
		
		Vector<IModelIndep> twitterModel = new Vector<IModelIndep>();
		twitterModel.add(new ModelIndepEventType());
		// Twitter: agent creation
		BayesAgentCellSimulatedEventType<IBayesRoundTable> twitterAgent = new BayesAgentCellSimulatedEventType<IBayesRoundTable>(twitterAgentCells, twitterModel);
		// Twitter: emitter
		ISA2RTBatchEmitter twitterEmitter= new ISA2RTBatchEmitter(roundTableManager);
		twitterAgent.setISA2RTemitter(twitterEmitter);
		// Twitter: simulation config
		long systemTimeIntervalMilli = systemTimeInterval.toDurationMillis();
		Duration simulationPeriod = new Duration(systemTimeIntervalMilli/1000);
		System.out.println("simulation period = " + simulationPeriod.getMillis() + " milliseconds");
		Duration twitterSamplingPeriod = new Duration(systemTimeInterval.toDurationMillis()/200);
		twitterAgent.setLastSampledTime(new Instant(startTime));
		twitterAgent.setSamplingPeriod(twitterSamplingPeriod);
		twitterAgent.setMyEvents(myEvents);
		//***************
		// Vodaphone like
		// data cnfig, cells and models
		CellsConstructor myCellConstructor = new CellsConstructor(myWorld);
		int subdiv=20;
		Vector<Area> vodaphoneAgentCells = myCellConstructor.makeLattice(subdiv);
		Vector<IModelIndep> vodaphoneModel = new Vector<IModelIndep>();
		for(int i=0; i< subdiv*subdiv; ++i)
			vodaphoneModel.add(new ModelIndepGaussian(0.0,1.0));
		// agent creation
		BayesAgentCellSimulated<IBayesRoundTable> vodaphoneAgent = new BayesAgentCellSimulated<IBayesRoundTable>(vodaphoneAgentCells, vodaphoneModel);
		// Vodaphone: emitter
		ISA2RTBatchEmitter vodaphoneEmitter= new ISA2RTBatchEmitter(roundTableManager);
		vodaphoneAgent.setISA2RTemitter(vodaphoneEmitter);
		// Vodaphone: simulation config
		Duration vodaphoneSamplingPeriod = new Duration(systemTimeInterval.toDurationMillis()/250);
		//TODO: fix this. At the moment, also need to change in ModelIndepGaussian
		vodaphoneAgent.setNoiseDistribution(0.0, 2.0);
		vodaphoneAgent.setLastSampledTime(new Instant(startTime));
		vodaphoneAgent.setSamplingPeriod(vodaphoneSamplingPeriod);
		vodaphoneAgent.setMyEvents(myEvents);
		
		//*****************************************
		// register agents to the RT manager
		roundTableManager.registerAgent(vodaphoneAgent);
		roundTableManager.registerAgent(twitterAgent);
		
		//*****************************************
		//***** Output
		//***************
		Data tmpItem= DataFactory.create();
		tmpItem.put("cells", twitterAgentCells);
		tmpItem.put("indepModels", twitterModel);
		//TODO:create file
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(System.getProperty("user.dir") + "/test/resources/simul1/Twitter.txt", "UTF-8");
			         //new File(System.getProperty("user.dir") + "/test/resources/simul1/Twitter.txt");
			String toOutput = JSONObject.toJSONString(tmpItem);
			Object obj=JSONValue.parse(toOutput);
			writer.println(toOutput);
			writer.close();
		} catch (Exception e) {
			log.error("could not write to {}", System.getProperty("user.dir") + "/test/resources/simul1/Twitter.txt");
			e.printStackTrace();
		}
	
		
		tmpItem= DataFactory.create();
		tmpItem.put("cells", vodaphoneAgentCells);
		tmpItem.put("indepModels", vodaphoneModel);
		//TODO:create file
		writer = null;
		try {
			writer = new PrintWriter(System.getProperty("user.dir") + "/test/resources/simul1/Phone.txt", "UTF-8");
			         //new File(System.getProperty("user.dir") + "/test/resources/simul1/Phone.txt");
			writer.println(JSONObject.toJSONString(tmpItem));
			writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//reload
		Data data = null;
		File file = null;
		try {
			file = new File(System.getProperty("user.dir") + "/test/resources/simul1/Phone.txt");
			JSONStream stream = new JSONStream(new SourceURL(file.toURI().toURL()));
			stream.init();
			data = stream.readNext();
		    } catch (Exception e) {
		      log.error("can't load from file! {}", file);
		      e.printStackTrace();
		}
//		
//		Serializable cellsValue = data.get("cells");
//		Object obj=JSONValue.parse(cellsValue.toString());
//		JSONArray array=(JSONArray)obj;
//		Vector<Area> agentCells = new Vector<Area>();
//		agentCells.ensureCapacity(array.size());
//		for(int ind=0; ind< array.size(); ++ind){
//			agentCells.add(Area.createFromJSON(array.get(ind).toString()));
//		}
//		System.out.println(agentCells);
//		Serializable cellsValue = data.get("indepModels");
//		Object obj=JSONValue.parse(cellsValue.toString());
//		JSONArray array=(JSONArray)obj;
//		Vector<IModelIndep> agentModels = new Vector<IModelIndep>();
//		agentModels.ensureCapacity(array.size());
//		for(int ind=0; ind< array.size(); ++ind){
//			agentModels.add(ModelIndepFactory.createFromJSON(array.get(ind).toString()));
//		}
//		System.out.println(agentModels);
//		Vector<IModelIndep> agentModels = JSONHelper.<IModelIndep,ModelIndepFactory>parseJsonArrayFromStringFactory(cellsValue.toString());
//		System.out.println(agentModels);
		//*****************************************
		//***** Simulate
		//***************
//		for(Instant simulationCurrentTime = new Instant(startTime);
//				simulationCurrentTime.isBefore(endTime);
//				simulationCurrentTime = simulationCurrentTime.plus(simulationPeriod)){
//			System.out.println("simulating time " + simulationCurrentTime);
//			twitterAgent.simulateAndAddData(simulationCurrentTime);
//			vodaphoneAgent.simulateAndAddData(simulationCurrentTime);
//		}
		
		
//		// Have a RT..
//		ProbabilisticRoundTable t = new ProbabilisticRoundTable();
//
//		printDebug();
//		initiateRandom(t);
//
//		// TODO: exchange sysout to logger
//		System.out.println("\n#############");
//
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//		Calendar c = new GregorianCalendar();
//		c.setTime(new Date());
//
//		for (int i = 0; i < 3; i++) {
//			ProbabilisticTestAgent a = new ProbabilisticTestAgent(); //instantiate test agent
//
//			//every agent get a data point
//			for (int j = 0; j < 1; j++) {
////				c.add(Calendar.HOUR, (int) (Math.random() * 8 - 4));
////				int randomLocation = (int) ((Math.random() * 9) - 2);
////				ProbabilisticRoundTableData issue = new ProbabilisticRoundTableData(
////						sdf.format(c.getTime()),
////						randomLocation,
////						(int) (Math.random() * 6 - 1));
////				a.addData(issue);
////				System.out.print("\n");
//			}
//			//agents are joined to the table
//			a.joinTable(t);
//			c.setTime(new Date());
//		}
//		t.discuss();
//		t.DataOnTable();
//		t.result();

	}

	private static void printDebug() {
//		String[] possibleLoc = "not used"; //ProbabilisticRoundTableData.possibleLoc;
//		String[] possibleEvent = "not used"; //ProbabilisticRoundTableData.possibleEvent;
//
//		System.out.println("possible locations are:");
//		for (String element : possibleLoc) {
//			System.out.print(element + " ");
//		}
//		System.out.println("\n");
//		for (String element : possibleEvent) {
//			System.out.print(element + " ");
//		}
//		System.out.println("\n");
	}

	// Move this to factory or sth like that..
	static private Random initiateRandom() {
		return new Random();
	}
}
