package de.fhg.insight.roundTable.Tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;

import de.fhg.insight.roundTable.IRoundTableAgent;
import de.fhg.insight.roundTable.RoundTable;
import de.fhg.insight.roundTable.Probabilistic.ProbabilisticRoundTable;
import de.fhg.insight.roundTable.Probabilistic.ProbabilisticRoundTableData;
import de.fhg.insight.roundTable.Probabilistic.ProbabilisticTestAgent;
import de.fhg.insight.roundTable.Samples.RoundTableDataExample;
import de.fhg.insight.roundTable.Samples.TestAgent;

/**
 * 
 * @author abablok
 * @author sbothe
 * 
 */
public class TestProbabilisticRoundTable {

	@Test
	public void simpleRoundTableSessionTest() {
		// Have a RT..
		ProbabilisticRoundTable t = new ProbabilisticRoundTable();

		printDebug();
		initiateRandom(t);

		// TODO: exchange sysout to logger
		System.out.println("\n#############");

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());

		for (int i = 0; i < 3; i++) {
			ProbabilisticTestAgent a = new ProbabilisticTestAgent(); //instantiate test agent

			//every agent get a data point
			for (int j = 0; j < 1; j++) {
//				c.add(Calendar.HOUR, (int) (Math.random() * 8 - 4));
//				int randomLocation = (int) ((Math.random() * 9) - 2);
//				ProbabilisticRoundTableData issue = new ProbabilisticRoundTableData(
//						sdf.format(c.getTime()),
//						randomLocation,
//						(int) (Math.random() * 6 - 1));
//				a.addData(issue);
//				System.out.print("\n");
			}
			//agents are joined to the table
			a.joinTable(t);
			c.setTime(new Date());
		}
		t.discuss();
		t.DataOnTable();
		t.result();

	}

	private static void printDebug() {
//		String[] possibleLoc = "not used"; //ProbabilisticRoundTableData.possibleLoc;
//		String[] possibleEvent = "not used"; //ProbabilisticRoundTableData.possibleEvent;
//
//		System.out.println("possible locations are:");
//		for (String element : possibleLoc) {
//			System.out.print(element + " ");
//		}
//		System.out.println("\n");
//		for (String element : possibleEvent) {
//			System.out.print(element + " ");
//		}
//		System.out.println("\n");
	}

	// Move this to factory or sth like that..
	static private void initiateRandom(ProbabilisticRoundTable t) {

		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//		ProbabilisticRoundTableData issue = new ProbabilisticRoundTableData(sdf.format(c
//				.getTime()), (int) (Math.random() * 10 - 1), -1);

//		IRoundTableAgent a = new ProbabilisticTestAgent(issue, "initiator"); //create new data point 
//		a.callTable(issue, t);
	}
}
