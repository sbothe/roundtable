package de.fhg.insight.roundTable.Tests;

import org.junit.Before;
import org.junit.Test;

import de.fhg.insight.roundTable.IIsaAgent;
import de.fhg.insight.roundTable.IRoundTable;
import de.fhg.insight.roundTable.IRoundTableData;
import de.fhg.insight.roundTable.Samples.RandomTestAgent;
import de.fhg.insight.roundTableService.RoundTableService;

public class ISARoundTableTest {

	@Before
	public void setUp() {
		for (int i = 10; i > 0; --i) {
			RoundTableService.registerAgent((IIsaAgent) new RandomTestAgent());
		}
	}

	@Test
	public void runRandomTable() {
		RandomTestAgent initiator = new RandomTestAgent();
		IRoundTable table = RoundTableService.createRoundTable(initiator);
		IRoundTableData result = table.discuss();
		System.out.println("=========================");
		System.out.println(result.toString());
	}

}