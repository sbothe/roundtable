#!/bin/sh

RTEnd=`cat log.txt | grep killing | wc -l`
RTEndShort=`cat log.txt | grep short | wc -l`
echo $RTEnd tables at the end: $RTEndShort too short

cat log.txt | grep "for issue" | sed -e 's/.*POLYGON\(.*\), type.*/\1/' -e 's/000000000000001//g' > anomalies.txt

cat log.txt | grep "event over. Conclusion" | sed -e 's/.*POLYGON\(.*\), type.*/\1/' > events.txt
